package com.muheda.invite.repository;

import com.muheda.invite.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by libin on 2018/7/14.
 */
public interface GroupRepository extends JpaRepository<Group,Long> {
    Group findByGroupUuid(String groupUuid) throws Exception;

    @Query("select a from Group a where (a.createUserUuid = ?1 or a.inviteUser1Uuid=?1 or a.inviteUser2Uuid=?1 ) and a.groupStatus in (1,2)")
    List<Group> findByUserUuidAndGroupStatus(String userUuid) throws Exception;

    int countByCreateUserUuidAndCreateDateAfter(String userUuid, Date start) throws Exception;

    int countByCreateUserUuidAndGroupStatus(String userUuid, int groupStatus) throws Exception;

    @Modifying
    @Transactional(propagation = Propagation.REQUIRED)
    @Query(nativeQuery = true,value = "update invite_group  set group_status =:groupStatus  where group_uuid=:groupUuid")
    void updateGroupStatusByGroupUuid(@Param("groupStatus") int groupStatus,@Param("groupUuid") String groupUuid)throws Exception;
}
