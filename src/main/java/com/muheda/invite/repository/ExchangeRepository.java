package com.muheda.invite.repository;

import com.muheda.invite.entity.ExchangeInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by libin on 2018/7/14.
 */
public interface ExchangeRepository extends JpaRepository<ExchangeInfo,Long> {
    List<ExchangeInfo> findByGroupIdAndUserUuid(int groupId, String userUuid);


    Page<ExchangeInfo> findAllByExchangeStatusAndUserUuid(int exchangeStatus, String userUuid, Pageable pageable);

    ExchangeInfo findByExchangeStatusAndUserUuid(int exchangeStatus, String userUuid);

    int countByExchangeStatusAndUserUuid(int exchangeStatus, String userUuid);

    ExchangeInfo findByGroupIdAndUserUuidAndExchangeType(int groupId, String userUuid, int exchangeType);

    List<ExchangeInfo> findByGroupUuidAndUserUuidAndExchangeStatus(String groupUuid, String userUuid,int exchangeStatus);


    int countByGroupUuidAndExchangeStatus(String groupUuid, int exchangeStatus);

    List<ExchangeInfo> findByGroupIdAndUserUuidAndExchangeStatus(int groupId,  String userUuid, int exchangeStatus);
}
