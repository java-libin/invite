package com.muheda.invite.repository;

import com.muheda.invite.entity.Partition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by libin on 2018/7/17.
 */
public interface PartitionRepository extends JpaRepository<Partition, Long> {

    @Modifying
    @Query(nativeQuery = true,value = "update invite_partition set partition_sum=partition_sum+:addSum where id=1")
    void updatePartition(@Param("addSum") int addSum);

    Partition findById(int id);
}
