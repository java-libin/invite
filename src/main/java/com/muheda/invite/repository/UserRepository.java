package com.muheda.invite.repository;

import com.muheda.invite.entity.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by libin on 2018/7/11.
 */
public interface UserRepository extends JpaRepository<User,Long> {
    User findByUserUuid(String userUuid) throws Exception;

    List<User> findTop20ByOrderByActivityPointDesc()throws Exception;

    User findByPhone(String phone)throws Exception;

    List<User> findTop5ByRealUserIsFalseOrderByActivityPointDesc()throws Exception;

    List<User> findTop20ByRealUserIsFalseOrderByActivityPointDesc()throws Exception;
}
