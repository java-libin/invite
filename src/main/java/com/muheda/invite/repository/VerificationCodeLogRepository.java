package com.muheda.invite.repository;

import com.muheda.invite.entity.VerificationCodeLog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by libin on 2018/7/14.
 */
public interface VerificationCodeLogRepository extends JpaRepository<VerificationCodeLog, Long> {

}
