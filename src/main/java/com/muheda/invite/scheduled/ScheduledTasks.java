package com.muheda.invite.scheduled;

import com.muheda.invite.common.util.HttpUtil;
import com.muheda.invite.common.util.WxUtil;
import com.muheda.invite.core.redis.RedisUtil;
import com.muheda.invite.entity.User;
import com.muheda.invite.repository.UserRepository;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by libin on 2018/5/28.
 */
@Component
public class ScheduledTasks {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private RedisUtil redisUtil;

    @Value("${wx.appid}")
    private String appid;

    @Value("${wx.secret}")
    private String secret;

    @Value("${wx.access-token.grant_type}")
    private String accessTokenGrantType;


    @Value("${wx.access-token.requestUrl}")
    private String accessTokenRequestUrl;

    @Value("${wx.access-token.redis-key}")
    private String accessTokenRedisKey;


    @Value("${wx.ticket.requestUrl}")
    private String ticketRequestUrl;

    @Value("${wx.ticket.redis-key}")
    private String ticketRedisKey;



    @Autowired
    private UserRepository userRepository;

    @Scheduled(cron="0 0 */2 * * *")
    public void getWxJsApi() {
        System.out.println("现在时间：" + dateFormat.format(new Date()));
        WxUtil.addWxInfoToRedis();
    }

    @Scheduled(cron="0 0 */6 * * *")
    public void activitySort() {
        System.out.println("现在时间： exec  activitySort" + dateFormat.format(new Date()));


        try {
            List<User> users = userRepository.findTop20ByRealUserIsFalseOrderByActivityPointDesc();
            if (users != null) {
                for (User user : users) {
                    if (user==null) continue;
                    int random = RandomUtils.nextInt(2, 4);
                    user.setActivityPoint(user.getActivityPoint()+random);
                    userRepository.save(user);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }


    }
}
