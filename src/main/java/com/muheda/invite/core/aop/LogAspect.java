package com.muheda.invite.core.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by libin on 2017/12/16.
 */
@Component
@Aspect
public class LogAspect {
    private Logger logger = LoggerFactory.getLogger(getClass());
    // 保证每个线程都有一个单独的实例
    private ThreadLocal<Long> startTime = new ThreadLocal();

    @Pointcut("execution(* com.muheda.invite.controller..*.*(..))")
    public void log() {
    }


    @Before("log()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        startTime.set(System.currentTimeMillis());
        if (attributes != null) {
            HttpServletRequest request = attributes.getRequest();
            //获取所有参数
//            Enumeration<String> enu = request.getParameterNames();
            StringBuilder builder = new StringBuilder();
//            while (enu.hasMoreElements()) {
//                String paraName = (String) enu.nextElement();
//                System.out.println(paraName + ": " + request.getParameter(paraName));
//                if (builder.length()==0){
//                    builder.append(paraName + ": " + request.getParameter(paraName));
//                }else {
//                    builder.append(" , "+paraName + " : " + request.getParameter(paraName));
//                }
//            }


            Object[] args = joinPoint.getArgs();
            for (Object arg : args) {

                if (builder.length() == 0) {
                    builder.append(arg);
                } else {
                    builder.append(" , " + arg);
                }
            }

            // 记录下请求内容
            logger.info("URL : {} , HTTP_METHOD : {} , IP : {} , CLASS_METHOD : {} , ARGS : {{}}",
                    request.getRequestURL().toString(),
                    request.getMethod(),
                    request.getRemoteAddr(),
                    joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName(),
//                    Arrays.toString(joinPoint.getArgs())
                    builder.toString()
            );
        }


    }


    @AfterReturning(pointcut = "log()", returning = "ret")
    public void afterReturning(JoinPoint joinPoint, Object ret) {
        //处理完请求，返回内容
        logger.info("CLASS_METHOD : {} , RESPONSE : {} , SPEND TIME : {} ms",
                joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName(),
                ret,
                (System.currentTimeMillis() - startTime.get())
        );

    }
}
