package com.muheda.invite.config;

import com.alibaba.fastjson.JSON;
import com.muheda.invite.common.result.Result;
import com.muheda.invite.common.result.ResultConst;
import com.muheda.invite.config.properties.JwtProperties;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;


//@WebFilter(urlPatterns = {"/activity*","/group/*","/invite/activity/*","/intive/group/*"},filterName = "ApiFilter")
//@Order(1)
@Component
public class ApiFilter implements Filter {

    private static Logger logger = LoggerFactory.getLogger(ApiFilter.class);

    @Autowired
    private JwtProperties jwtProperties;


    private static List<String> allowIps;

    @Value("${jwt.allow-ips}")
    public void setAllowIps(String allowIps) {
        ApiFilter.allowIps = Arrays.asList(allowIps.split(","));
    }


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse rep = (HttpServletResponse) response;
        System.out.println(allowIps);
        System.out.println(req.getHeader("Origin"));
        //设置允许跨域的配置
        // 这里填写你允许进行跨域的主机ip（正式上线时可以动态配置具体允许的域名和IP）
//        rep.setHeader("Access-Control-Allow-Origin", "*");
        if (allowIps.contains(req.getHeader("Origin"))) {
            System.out.println("here set Access-Control-Allow-Origin");
            rep.setHeader("Access-Control-Allow-Origin", req.getHeader("Origin"));
        }
        System.out.println(rep.getHeader("Access-Control-Allow-Origin"));
        // 允许的访问方法
        rep.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE, PATCH");
        // Access-Control-Max-Age 用于 CORS 相关配置的缓存
        rep.setHeader("Access-Control-Max-Age", "3600");
        rep.setHeader("Access-Control-Allow-Headers", "token,Origin, X-Requested-With, Content-Type, Accept,Authorization");
        rep.setHeader("Access-Control-Allow-Credentials", "true");


        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        System.out.println(111);

        System.out.println(jwtProperties == null);
        System.out.println(((HttpServletRequest) request).getMethod());
        System.out.println(jwtProperties.getHeader());

        String token = req.getHeader(jwtProperties.getHeader());//header方式
        Result resultInfo = new Result();
        boolean isFilter = false;


        String method = ((HttpServletRequest) request).getMethod();
        if (method.equals("OPTIONS")) {
            rep.setStatus(HttpServletResponse.SC_OK);
        } else {

            /**
             * 判断是否有token，没有直接返回客户端
             */
            if (null == token || token.isEmpty()) {
                resultInfo.setCode(ResultConst.TOKEN_INVALID.getCode());
                resultInfo.setMessage("用户授权认证没有通过!客户端请求参数中无token信息");
                resultInfo.setData("");
            } else {
                try {

                    System.out.println(Jwts.parser().setSigningKey(jwtProperties.getSecret()).parseClaimsJws(token).getBody().get("userUuid"));

                    String user = Jwts.parser()
                            .setSigningKey(jwtProperties.getSecret())
                            .parseClaimsJws(token)
                            .getBody()
                            .getSubject();
                    if (user != null) {
                        resultInfo.setCode(0);
                        isFilter = true;
                    } else {
                        resultInfo.setCode(ResultConst.TOKEN_INVALID.getCode());
                        resultInfo.setMessage("用户授权认证没有通过!客户端请求参数token信息无效");
                        resultInfo.setData("");
                    }
                } catch (Exception e) {
                    resultInfo.setCode(ResultConst.TOKEN_INVALID.getCode());
                    resultInfo.setMessage("用户授权认证没有通过!客户端请求参数token信息无效");
                    resultInfo.setData("");
                }

            }


            /**
             * 验证失败向客户端返回内容
             */
            if (resultInfo.getCode() != 0) {// 验证失败
                PrintWriter writer = null;
                OutputStreamWriter osw = null;
                try {
                    osw = new OutputStreamWriter(response.getOutputStream(), "UTF-8");
                    writer = new PrintWriter(osw, true);
                    String jsonStr = JSON.toJSONString(resultInfo);
                    writer.write(jsonStr);
                    writer.flush();
                    writer.close();
                    osw.close();
                } catch (UnsupportedEncodingException e) {
                    logger.error("过滤器返回信息失败:" + e.getMessage(), e);
                } catch (IOException e) {
                    logger.error("过滤器返回信息失败:" + e.getMessage(), e);
                } finally {
                    if (null != writer) {
                        writer.close();
                    }
                    if (null != osw) {
                        osw.close();
                    }
                }
                return;
            }

            /**
             * 过滤器成功：执行业务逻辑
             */
            if (isFilter) {
                chain.doFilter(request, response);
            }
        }


    }

    @Override
    public void destroy() {
    }
}
