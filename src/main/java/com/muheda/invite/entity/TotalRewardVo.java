package com.muheda.invite.entity;

/**
 * Created by libin on 2018/7/21.
 */
public class TotalRewardVo {

    private int activityPoint;
    private int activityPointCoupon;



    public int getActivityPoint() {
        return activityPoint;
    }

    public void setActivityPoint(int activityPoint) {
        this.activityPoint = activityPoint;
    }

    public int getActivityPointCoupon() {
        return activityPointCoupon;
    }

    public void setActivityPointCoupon(int activityPointCoupon) {
        this.activityPointCoupon = activityPointCoupon;
    }
}
