package com.muheda.invite.entity;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by libin on 2018/7/17.
 */
@Entity
@Table(name = "invite_partition")
@EntityListeners(AuditingEntityListener.class)
public class Partition implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id ;//


    private Integer partitionSum;//已经瓜分红宝数


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPartitionSum() {
        return partitionSum;
    }

    public void setPartitionSum(Integer partitionSum) {
        this.partitionSum = partitionSum;
    }

    @Override
    public String toString() {
        return "Partition{" +
                "id=" + id +
                ", partitionSum=" + partitionSum +
                '}';
    }
}
