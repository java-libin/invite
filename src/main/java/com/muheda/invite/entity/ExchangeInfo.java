package com.muheda.invite.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by libin on 2018/7/13.
 */
@Entity
@Table(name = "exchange_info")
@EntityListeners(AuditingEntityListener.class)
public class ExchangeInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

//    @Column(nullable = false)
    private Integer userId;//  用户id

    @Column(nullable = false)
    private String userUuid; //用户uuid


//    @Column(nullable = false)
    private Integer groupId;//组团id

//    @Column(nullable = false)
    private String groupUuid;//组团 uuid

    private Integer activityPointSum =0;//兑换点数

    private Integer activityPointCouponSum =0;//兑换点券数量

    private Integer exchangeStatus=0;//0 未兑换  1 已兑换

    private Integer exchangeType;//1 组团 团长奖励 2 随机瓜分

    private Integer userType ;//1 :团成员 2 团长

    @CreatedDate
    private Date createDate;//记录创建时间

    private Date exchangeDate;//兑换时间

    @JsonIgnore
    private boolean realUserData = true;//是否真实用户数据


    public String getGroupUuid() {
        return groupUuid;
    }

    public void setGroupUuid(String groupUuid) {
        this.groupUuid = groupUuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }


    public Integer getActivityPointSum() {
        return activityPointSum;
    }

    public void setActivityPointSum(Integer activityPointSum) {
        this.activityPointSum = activityPointSum;
    }

    public Integer getActivityPointCouponSum() {
        return activityPointCouponSum;
    }

    public void setActivityPointCouponSum(Integer activityPointCouponSum) {
        this.activityPointCouponSum = activityPointCouponSum;
    }

    public Integer getExchangeStatus() {
        return exchangeStatus;
    }

    public void setExchangeStatus(Integer exchangeStatus) {
        this.exchangeStatus = exchangeStatus;
    }

    public Integer getExchangeType() {
        return exchangeType;
    }

    public void setExchangeType(Integer exchangeType) {
        this.exchangeType = exchangeType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getExchangeDate() {
        return exchangeDate;
    }

    public void setExchangeDate(Date exchangeDate) {
        this.exchangeDate = exchangeDate;
    }

    public boolean isRealUserData() {
        return realUserData;
    }

    public void setRealUserData(boolean realUserData) {
        this.realUserData = realUserData;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "ExchangeInfo{" +
                "id=" + id +
                ", userId=" + userId +
                ", userUuid='" + userUuid + '\'' +
                ", groupId=" + groupId +
                ", groupUuid='" + groupUuid + '\'' +
                ", activityPointSum=" + activityPointSum +
                ", activityPointCouponSum=" + activityPointCouponSum +
                ", exchangeStatus=" + exchangeStatus +
                ", exchangeType=" + exchangeType +
                ", userType=" + userType +
                ", createDate=" + createDate +
                ", exchangeDate=" + exchangeDate +
                ", realUserData=" + realUserData +
                '}';
    }
}
