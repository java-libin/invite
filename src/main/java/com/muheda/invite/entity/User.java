package com.muheda.invite.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by libin on 2018/7/11.
 */
@Entity
@Table(name = "invite_user")
@EntityListeners(AuditingEntityListener.class)
public class User implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;//活动用户id

    private String userName;//用户姓名

    @Column(nullable = false, unique = true)
    private String userUuid;

    @Column(nullable = false, unique = true)
    private String phone;//手机号


    private Integer status = 1;//状态值 默认1 有效 0 无效

    private boolean newRegUser; //是否新注册用户
    private boolean activityRegUser; //是否活动注册用户


    private boolean realUser = true;//是否真实用户

    private int activityPoint; // 点 总数
    private int activityPointCoupon; // 点券

    private String inviteUserUuid;//邀请人 uuid
    @Transient
    private int activityAmount;//活动总金额


    @Transient
    private String hiddenPhone;//隐藏中间四位手机号

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;


    @LastModifiedDate
    private Date lastUpdateDate;

    public String getHiddenPhone() {
        return this.phone.replace(this.phone.substring(3, 7), "****");
    }


    public int getActivityAmount() {

        int temp = 0;
        if (activityPoint > 0) {
            temp += activityPoint;
        }
        if (activityPointCoupon > 0) {
            temp += activityPointCoupon;
        }
        if (temp > 0) {
            return temp * 2;
        }
        return 0;
    }

    public void setActivityAmount(int activityAmount) {

        this.activityAmount = activityAmount;
    }

    public boolean isRealUser() {
        return realUser;
    }

    public void setRealUser(boolean realUser) {
        this.realUser = realUser;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isActivityRegUser() {
        return activityRegUser;
    }

    public void setActivityRegUser(boolean activityRegUser) {
        this.activityRegUser = activityRegUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public boolean isNewRegUser() {
        return newRegUser;
    }

    public void setNewRegUser(boolean newRegUser) {
        this.newRegUser = newRegUser;
    }


    public int getActivityPoint() {
        return activityPoint;
    }

    public void setActivityPoint(int activityPoint) {
        this.activityPoint = activityPoint;
    }

    public int getActivityPointCoupon() {
        return activityPointCoupon;
    }

    public void setActivityPointCoupon(int activityPointCoupon) {
        this.activityPointCoupon = activityPointCoupon;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getInviteUserUuid() {
        return inviteUserUuid;
    }

    public void setInviteUserUuid(String inviteUserUuid) {
        this.inviteUserUuid = inviteUserUuid;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", userUuid='" + userUuid + '\'' +
                ", phone='" + phone + '\'' +
                ", status=" + status +
                ", newRegUser=" + newRegUser +
                ", realUser=" + realUser +
                ", activityPoint=" + activityPoint +
                ", activityPointCoupon=" + activityPointCoupon +
                ", inviteUserUuid='" + inviteUserUuid + '\'' +
                ", createDate=" + createDate +
                ", lastUpdateDate=" + lastUpdateDate +
                '}';
    }


    public static void main(String[] args) {
        System.out.println("18311367678".subSequence(3, 7));
    }
}
