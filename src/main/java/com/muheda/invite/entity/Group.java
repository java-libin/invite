package com.muheda.invite.entity;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by libin on 2018/7/12.
 */
@Entity
@Table(name = "invite_group")
@EntityListeners(AuditingEntityListener.class)
public class Group  implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String createUserUuid;//团队创建人 uuid
    private String createUserPhone;//团队创建人 手机号

    private String groupUuid;//组团  uuid



    private String inviteUser1Uuid;//邀请第一个用户  uuid
    private String inviteUser2Uuid;//邀请第二个用户  uuid

    private String inviteUser1Phone;//邀请第一个用户  手机号
    private String inviteUser2Phone;//邀请第二个用户  手机号


    @CreatedDate
    private Date createDate;//组团创建时间

    private Date inviteUser1JoinDate;//邀请第一个用户参团时间
    private Date inviteUser2JoinDate;//邀请第二个用户参团时间

    private int groupStatus=1;//状态 1 有效  0 无效 2 组团成功

    private Date groupSuccessDate;//组团成功时间


    @Transient
    private List<ExchangeInfo> userNotExchangeInfos;//用户未兑换奖励 兑换状态为0 此字段有数据

    @Transient
    private int userExchangeStatus;//用户兑换状态 0 未兑换 1 兑换


    public List<ExchangeInfo> getUserNotExchangeInfos() {
        return userNotExchangeInfos;
    }

    public void setUserNotExchangeInfos(List<ExchangeInfo> userNotExchangeInfos) {
        this.userNotExchangeInfos = userNotExchangeInfos;
    }

    public int getUserExchangeStatus() {
        return userExchangeStatus;
    }

    public void setUserExchangeStatus(int userExchangeStatus) {
        this.userExchangeStatus = userExchangeStatus;
    }

    public String getInviteUser1Phone() {
        return inviteUser1Phone;
    }

    public void setInviteUser1Phone(String inviteUser1Phone) {
        this.inviteUser1Phone = inviteUser1Phone;
    }

    public String getInviteUser2Phone() {
        return inviteUser2Phone;
    }

    public void setInviteUser2Phone(String inviteUser2Phone) {
        this.inviteUser2Phone = inviteUser2Phone;
    }

    public String getCreateUserPhone() {
        return createUserPhone;
    }

    public void setCreateUserPhone(String createUserPhone) {
        this.createUserPhone = createUserPhone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getCreateUserUuid() {
        return createUserUuid;
    }

    public void setCreateUserUuid(String createUserUuid) {
        this.createUserUuid = createUserUuid;
    }

    public String getGroupUuid() {
        return groupUuid;
    }

    public void setGroupUuid(String groupUuid) {
        this.groupUuid = groupUuid;
    }

    public String getInviteUser1Uuid() {
        return inviteUser1Uuid;
    }

    public void setInviteUser1Uuid(String inviteUser1Uuid) {
        this.inviteUser1Uuid = inviteUser1Uuid;
    }

    public String getInviteUser2Uuid() {
        return inviteUser2Uuid;
    }

    public void setInviteUser2Uuid(String inviteUser2Uuid) {
        this.inviteUser2Uuid = inviteUser2Uuid;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getInviteUser1JoinDate() {
        return inviteUser1JoinDate;
    }

    public void setInviteUser1JoinDate(Date inviteUser1JoinDate) {
        this.inviteUser1JoinDate = inviteUser1JoinDate;
    }

    public Date getInviteUser2JoinDate() {
        return inviteUser2JoinDate;
    }

    public void setInviteUser2JoinDate(Date inviteUser2JoinDate) {
        this.inviteUser2JoinDate = inviteUser2JoinDate;
    }

    public int getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(int groupStatus) {
        this.groupStatus = groupStatus;
    }

    public Date getGroupSuccessDate() {
        return groupSuccessDate;
    }

    public void setGroupSuccessDate(Date groupSuccessDate) {
        this.groupSuccessDate = groupSuccessDate;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", createUserUuid='" + createUserUuid + '\'' +
                ", groupUuid='" + groupUuid + '\'' +
                ", inviteUser1Uuid='" + inviteUser1Uuid + '\'' +
                ", inviteUser2Uuid='" + inviteUser2Uuid + '\'' +
                ", createDate=" + createDate +
                ", inviteUser1JoinDate=" + inviteUser1JoinDate +
                ", inviteUser2JoinDate=" + inviteUser2JoinDate +
                ", groupStatus=" + groupStatus +
                ", groupSuccessDate=" + groupSuccessDate +
                '}';
    }
}
