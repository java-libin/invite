package com.muheda.invite.entity;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by libin on 2018/7/14.
 */
@Entity
@Table(name = "verification_code_log")
@EntityListeners(AuditingEntityListener.class)
public class VerificationCodeLog implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;//验证码信息id

    @CreatedDate
    private Date createDate;

    private String phone;

    private String msgContent;

    private String respCode;

    private int messageType;//1 sms 2 voice

    @Column(columnDefinition = "text")
    private String exception;

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }


    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    @Override
    public String toString() {
        return "VerificationCodeLog{" +
                "id=" + id +
                ", createDate=" + createDate +
                ", phone='" + phone + '\'' +
                ", msgContent='" + msgContent + '\'' +
                ", respCode='" + respCode + '\'' +
                ", messageType=" + messageType +
                ", exception='" + exception + '\'' +
                '}';
    }
}
