package com.muheda.invite.common.util;

import com.muheda.invite.core.redis.RedisUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by libin on 2018/7/19.
 */
@Component
public class WxUtil {

    private static RedisUtil redisUtil;


    private static String accessTokenGrantType;


    private static String appid;


    private static String secret;


    private static String accessTokenRequestUrl;


    private static String accessTokenRedisKey;


    private static String ticketRequestUrl;

    private static String ticketRedisKey;

    @Value("${wx.access-token.grant_type}")
    public void setAccessTokenGrantType(String accessTokenGrantType) {
        this.accessTokenGrantType = accessTokenGrantType;
    }

    @Value("${wx.appid}")
    public void setAppid(String appid) {
        this.appid = appid;
    }

    @Value("${wx.secret}")
    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Value("${wx.access-token.requestUrl}")
    public void setAccessTokenRequestUrl(String accessTokenRequestUrl) {
        this.accessTokenRequestUrl = accessTokenRequestUrl;
    }

    @Value("${wx.access-token.redis-key}")
    public void setAccessTokenRedisKey(String accessTokenRedisKey) {
        this.accessTokenRedisKey = accessTokenRedisKey;
    }

    @Value("${wx.ticket.requestUrl}")
    public void setTicketRequestUrl(String ticketRequestUrl) {
        this.ticketRequestUrl = ticketRequestUrl;
    }

    @Value("${wx.ticket.redis-key}")
    public void setTicketRedisKey(String ticketRedisKey) {
        this.ticketRedisKey = ticketRedisKey;
    }

    @Autowired
    public void setRedisUtil(RedisUtil redisUtil) {
        this.redisUtil = redisUtil;
    }

    public static  String addWxInfoToRedis() {
        try {
            Map<String, Object> requestMap = new HashMap<>();
            requestMap.put("appid", appid);
            requestMap.put("secret", secret);
            requestMap.put("grant_type", accessTokenGrantType);
            Map<String, Object> accessTokenByMap = HttpUtil.requestByMap(accessTokenRequestUrl, requestMap);
            if (accessTokenByMap != null && StringUtils.isNotEmpty((String) accessTokenByMap.get("access_token"))) {
                redisUtil.set(accessTokenRedisKey + appid, (String) accessTokenByMap.get("access_token"), 7200);

                requestMap = new HashMap<>();
                requestMap.put("access_token", (String) accessTokenByMap.get("access_token"));
                requestMap.put("type", "jsapi");
                Map<String, Object> ticketMap = HttpUtil.requestByMap(ticketRequestUrl, requestMap);
                if (ticketMap != null && (int) ticketMap.get("errcode") == 0) {
                    redisUtil.set(ticketRedisKey + appid, (String) ticketMap.get("ticket"), 7200);
                    return  (String) ticketMap.get("ticket");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }
}
