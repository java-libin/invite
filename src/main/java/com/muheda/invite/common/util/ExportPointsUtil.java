package com.muheda.invite.common.util;

import java.util.*;

/**
 * @author muheda
 */
public class ExportPointsUtil {
	
	/**
	 * 创建md5摘要,规则是:按参数名称a-z排序,遇到空值的参数不参加签名。
	 */
	public static String createSign(Map<String, String> paramMap, String secretKey) {

		StringBuffer sb = new StringBuffer();
		List<String> keyList = new ArrayList<String>(paramMap.keySet());
		Collections.sort(keyList);

		for(String key:keyList){
			String value = paramMap.get(key);
			if(!"sign".equals(key) && null != value && !"".equals(value)){
				sb.append(key + "=" + value + "&");
			}
		}
		sb.append("secretKey=" + secretKey);

		String sign = MD5Util.MD5Encode(sb.toString(), "UTF-8").toUpperCase();
		return sign;
	}




}
