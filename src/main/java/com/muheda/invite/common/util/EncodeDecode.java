package com.muheda.invite.common.util;

import com.alibaba.fastjson.JSONObject;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.net.URLDecoder;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author muheda
 */
public class EncodeDecode {

	public static final String ALGORITHM_DES = "DES/CBC/PKCS5Padding";

	  /**
	   * DES算法，加密
	   * @param data 待加密字符串
	   * @param key 加密私钥，长度不能够小于8位
	   * @return 加密后的字节数组，一般结合Base64编码使用
	   */
	  public static String encode(String key,String data) throws Exception {
	    return encode(key, data.getBytes());
	  }

	  /**
	   * DES算法，加密
	   *
	   * @param data 待加密字符串
	   * @param key 加密私钥，长度不能够小于8位
	   * @return 加密后的字节数组，一般结合Base64编码使用
	   */
	  public static String encode(String key,byte[] data) throws Exception {
	    try {
          DESKeySpec dks = new DESKeySpec(key.getBytes());
          SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
	      //key的长度不能够小于8位字节
	      Key secretKey = keyFactory.generateSecret(dks);
	      Cipher cipher = Cipher.getInstance(ALGORITHM_DES);
	      IvParameterSpec iv = new IvParameterSpec(key.getBytes());
	      AlgorithmParameterSpec paramSpec = iv;
	      cipher.init(Cipher.ENCRYPT_MODE, secretKey, paramSpec);
	      byte[] bytes = cipher.doFinal(data);
	      return Base64.encode(bytes);
	    } catch (Exception e) {
	      throw new Exception(e);
	    }
	  }
	  
	  /**
	   * DES算法，解密
	   *
	   * @param data 待解密字符串
	   * @param key 解密私钥，长度不能够小于8位
	   * @return 解密后的字节数组
	   * @throws Exception 异常
	   */
	  public static byte[] decode(String key,byte[] data) throws Exception {
	    try {
	      //SecureRandom sr = new SecureRandom();
	      DESKeySpec dks = new DESKeySpec(key.getBytes());
	      SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
	      //key的长度不能够小于8位字节
	      Key secretKey = keyFactory.generateSecret(dks);
	      Cipher cipher = Cipher.getInstance(ALGORITHM_DES);
	      IvParameterSpec iv = new IvParameterSpec(key.getBytes());
	      AlgorithmParameterSpec paramSpec = iv;
	      cipher.init(Cipher.DECRYPT_MODE, secretKey,paramSpec);
	      return cipher.doFinal(data);
	    } catch (Exception e) {
	      throw new Exception(e);
	    }
	  }
	  
	  /**
	   * 获取编码后的值
	   * @param key
	   * @param data
	   * @return
	   * @throws Exception
	   */
      public static String decodeValue(String key, String data) {
          byte[] datas;
          String value = null;
          try {
              datas = decode(key, Base64.decode(data));
              value = new String(datas);
          }catch (Exception d){
              try {
                  data = URLDecoder.decode(data, "utf-8");
                  datas = decode(key, Base64.decode(data));
                  value = new String(datas);
              } catch (Exception e) {
                  e.printStackTrace();
              }
          }
          return value;
      }


	public static void main(String[] args) throws Exception {
		String encodeKey ="npullpwd";
		String signKey="newpullappkey0000000000000000000";
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("uuid", "ac3ab727-910a-4d4e-8bcf-adb9de8efc3c");


		//Map<String, String> encryptMap = com.muheda.uc.util.EncryptDecrypt.Encrypt(paramMap, encodeKey, signKey);
		//System.out.println(JSONObject.toJSONString(encryptMap));

		//String resultData = encryptMap.get("resultData");

//		String resultData = "jZjhFarrCTGHTNJTU4KrR0SwVYInasm7og4rJqUrRqQgWc46mBIsG%2F3iwT%2BNCn0YrNExb6GWIwyRInziwk4lBa4b6balyupUA3AyNe6M6ib%2FeAZvU%2F7DeK484r273Brj";
		String resultData = "jZ4fJgcM3z4W5CFadLF8mPDadZLo8K7DsJTCUvjad9OOUXh9hp5hgNtxDBtkQ5hhHaatzTu5pXajlOUB9FybTSQEWHlIE9Ed3CkOxuXvbrAnQJZ%2FNs1xdoIShwIAhqr%2F";
		resultData = URLDecoder.decode(resultData, "utf-8");
		String retString = decodeValue(encodeKey, resultData);
		System.out.println(retString);

		//Map<String, Object> decryptMap = com.muheda.uc.util.EncryptDecrypt.Decrypt(resultData, encodeKey, signKey);
		//System.out.println(JSONObject.toJSONString(decryptMap));
	}
}
