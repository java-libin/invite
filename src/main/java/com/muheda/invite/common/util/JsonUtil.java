package com.muheda.invite.common.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.muheda.invite.common.exception.BaseException;
import com.muheda.invite.common.result.ResultConst;

import java.util.Map;

/**
 * Created by libin on 2018/7/16.
 */
public class JsonUtil {


    /**
     * 将json转化成map
     *
     * @param jsonStr
     * @return
     */
    public static Map<String, Object> convertJsonStrToMap(String jsonStr) {

        Map<String, Object> map = JSON.parseObject(
                jsonStr, new TypeReference<Map<String, Object>>() {
                });

        return map;

    }

    public static Map<String, String> convertJsonStrToStringMap(String jsonStr) {

        Map<String, String> map = JSON.parseObject(
                jsonStr, new TypeReference<Map<String, String>>() {
                });

        return map;

    }

    public static void main(String[] args) {
        String result = "{\n" +
                "  \"success\": false,\n" +
                "  \"code\": \"200\",\n" +
                "  \"message\": \"请求成功\",\n" +
                "  \"data\": {\n" +
                "    \"uuid\": \"e64131c4-b39d-47e7-9894-4723d5ea5e2b\"\n" +
                "  }\n" +
                "}";
        Map<String, Object> resultMap = JsonUtil.convertJsonStrToMap(result);
        if (resultMap == null || resultMap.size() == 0) {
            System.out.println(1);

        }

        System.out.println(((Map)resultMap.get("data")).get("uuid"));
        if ((Boolean)resultMap.get("success")) {
            System.out.println(resultMap.get("success"));
        } else {
            System.out.println(Integer.parseInt(String.valueOf(resultMap.get("code")))+"-----"+ (String)resultMap.get("message"));
        }
    }


}