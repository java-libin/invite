package com.muheda.invite.common.util;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;

/**
 * @author muheda
 */
public class EncryptDecrypt {

	private static Logger logger = LoggerFactory.getLogger(EncryptDecrypt.class);

    /**
     * @Descripton: 加密方法
     * @Author: Sorin
     * @param paramMap
     * @param encodeKey
     * @param signKey
     * @Date: 2018/7/17
     */
	public static Map<String, String> encrypt(Map<String, String> paramMap, String encodeKey, String signKey) {


		Map<String, String> retMap = new HashMap<String, String>();
		try {
			if (StringUtils.isEmpty(encodeKey) || encodeKey.length() != 8) {
				// encodeKey为空或者长度不是8位！
				logger.error("系统参数错误： DES秘钥encodeKey为空或者长度不是8位！");
				retMap.put("resultCode", "002");
				retMap.put("resultMessage", "系统参数错误： DES秘钥encodeKey为空或者长度不是8位！");
				return retMap;
			}
			if (StringUtils.isEmpty(signKey)) {
				// 系统参数错误
				logger.error("系统参数错误：sign签名秘钥为空！");
				retMap.put("resultCode", "003");
				retMap.put("resultMessage", "系统参数错误：sign签名秘钥为空！");
				return retMap;
			}

			if (null == paramMap || paramMap.isEmpty()) {
				// 系统参数错误
				logger.error("系统参数错误：传入参数为空！");
				retMap.put("resultCode", "004");
				retMap.put("resultMessage", "系统参数错误：传入参数为空！");
				return retMap;
			}
			String sign = ExportPointsUtil.createSign(paramMap, signKey);
			paramMap.put("sign", sign);

//			JSONObject paramsJson = JSONObject.fromObject(paramMap);
			String jsonStr = JSON.toJSONString( paramMap );

			String retString = EncodeDecode.encode(encodeKey, jsonStr);
			retString = URLEncoder.encode(retString, "utf-8");
			// 加密数据成功
			retMap.put("resultCode", "000");
			retMap.put("resultData", retString);
			return retMap;
		} catch (Exception e) {
			// 系统异常
			logger.error("系统异常！");
			e.printStackTrace();
			retMap.put("resultCode", "001");
			retMap.put("resultMessage", "系统异常！");
			return retMap;
		}
	}

    /**
     * @Descripton: 解密方法
     * @Author: Sorin
     * @param params
     * @param encodeKey
     * @param signKey
     * @Date: 2018/7/17
     */
	public static Map<String, String> decrypt(String params, String encodeKey, String signKey) {
		Map<String, String> retMap = new HashMap<String, String>();
		try {
			if (StringUtils.isEmpty(encodeKey) || encodeKey.length() != 8) {
				// encodeKey为空或者长度不是8位！
				logger.error("系统参数错误： DES秘钥encodeKey为空或者长度不是8位！");
				retMap.put("resultCode", "002");
				retMap.put("resultMessage", "系统参数错误： DES秘钥encodeKey为空或者长度不是8位！");
				return retMap;
			}

			if (StringUtils.isEmpty(signKey)) {
				// 系统参数错误
				logger.error("系统参数错误：sign签名秘钥为空！");
				retMap.put("resultCode", "003");
				retMap.put("resultMessage", "系统参数错误：sign签名秘钥为空！");
				return retMap;
			}

			if (StringUtils.isEmpty(params)) {
				// 系统参数错误
				logger.error("系统参数错误：传入参数为空！");
				retMap.put("resultCode", "004");
				retMap.put("resultMessage", "系统参数错误：传入参数为空！");
				return retMap;
			}



			params = new String(params.getBytes("ISO-8859-1"), "UTF-8");




			// 参数DES解密
			String paramsJsonString = EncodeDecode.decodeValue(encodeKey, params);
			if(StringUtils.isEmpty(paramsJsonString)){
				return null;
			}
//
			Map<String, String> paramMap = JsonUtil.convertJsonStrToStringMap(paramsJsonString);

//			JSONObject paramsJson = JSONObject.fromObject(paramsJsonString);
//
//			SortedMap<String, String> paramMap = new TreeMap<String, String>();
//
//			Iterator it = paramsJson.keys();
//			// 遍历jsonObject数据，添加到Map对象
//			try {
//				while (it.hasNext()) {
//					String key = String.valueOf(it.next());
//					String value = (String) paramsJson.get(key);
//					paramMap.put(key, value);
//				}
//			} catch (Exception e) {
//				// 系统参数错误
//				logger.error("系统参数错误：传入参数解密后为空！");
//				retMap.put("resultCode", "005");
//				retMap.put("resultMessage", "系统参数错误：传入参数解密后为空！");
//				return retMap;
//			}



			
			if (paramMap == null || paramMap.isEmpty()) {
				// 系统参数错误
				logger.error("系统参数错误：传入参数解密后为空！");
				retMap.put("resultCode", "005");
				retMap.put("resultMessage", "系统参数错误：传入参数解密后为空！");
				return retMap;
			}

			// 获取合法签名
			String sign = ExportPointsUtil.createSign(paramMap, signKey);

			// 验证签名是否正确
			if (!sign.equals(paramMap.get("sign"))) {
				// 请求验证错误
				logger.error("请求验证错误！");
				retMap.put("resultCode", "006");
				retMap.put("resultMessage", "请求验证错误！");
				return retMap;
			}

			// 解析参数成功
			retMap.put("resultCode", "000");
			retMap.put("resultData", paramMap.get("uuid"));
			return retMap;
		} catch (Exception e) {
			// 系统异常
			logger.error("系统异常！");
			e.printStackTrace();
			retMap.put("resultCode", "001");
			retMap.put("resultMessage", "系统异常！");
			return retMap;
		}
	}
}
