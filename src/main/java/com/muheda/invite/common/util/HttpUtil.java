package com.muheda.invite.common.util;


import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * HTTP请求对象
 *
 * @author
 */
public class HttpUtil {

    public static String http(String url, Map<String, String> params) {


        URL u = null;

        HttpURLConnection con = null;

        // 尝试发送请求

        try {

            u = new URL(url);

            con = (HttpURLConnection) u.openConnection();
            if (params != null) {
                for (Entry<String, String> e : params.entrySet()) {
                    con.addRequestProperty(e.getKey(), e.getValue());
                }
            }


            con.setRequestMethod("POST");

            con.setDoOutput(true);

            con.setDoInput(true);

            con.setUseCaches(false);

//			con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

            OutputStreamWriter osw = new OutputStreamWriter(
                    con.getOutputStream(), "UTF-8");

//			osw.write(sb.toString());

            osw.flush();

            osw.close();

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            if (con != null) {

                con.disconnect();

            }

        }

        // 读取返回内容

        StringBuffer buffer = new StringBuffer();

        try {

            BufferedReader br = new BufferedReader(new InputStreamReader(con

                    .getInputStream(), "UTF-8"));

            String temp;

            while ((temp = br.readLine()) != null) {

                buffer.append(temp);

                buffer.append("\n");

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return buffer.toString();

    }

    public static String doPostJson(String url, String json) {

        URL u = null;

        HttpURLConnection con = null;


        try {

            u = new URL(url);

            con = (HttpURLConnection) u.openConnection();

            con.setRequestMethod("POST");

            con.setDoOutput(true);

            con.setDoInput(true);

            con.setUseCaches(false);

            con.setRequestProperty("Content-Type", "application/json");

            OutputStreamWriter osw = new OutputStreamWriter(con.getOutputStream(), "UTF-8");

            osw.write(json);

            osw.flush();

            osw.close();

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            if (con != null) {

                con.disconnect();

            }

        }

        // 读取返回内容

        StringBuffer buffer = new StringBuffer();

        try {

            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));

            String temp;

            while ((temp = br.readLine()) != null) {

                buffer.append(temp);

                buffer.append("\n");

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return buffer.toString();
    }

    public static String sendPost(String url, String urlParameters) {
        StringBuffer response = new StringBuffer();
        int responseCode = -1;
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            System.out.println(urlParameters);
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
            responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine = "";
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            System.out.println("responseCode: " + responseCode + "\t" + "POST" + "\trequestUrl: " + url + "\tresponse: " + response.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.toString();
    }


    public static Map<String, Object> requestByMap(String url, Map<String, Object> requestMap) throws Exception {


        StringBuffer buffer = new StringBuffer();
        for (Map.Entry<String, Object> entry : requestMap.entrySet()) {
            buffer.append(entry.getKey()).append("=").append(entry.getValue() + "&");
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
        }

        Map<String, Object> responseMap = null;
        CloseableHttpClient httpClient = new SSLClient();


        HttpPost httpPost = new HttpPost(url);
        System.out.println(buffer.toString().substring(0, buffer.toString().length() - 1));
        StringEntity requestEntity = new StringEntity(buffer.toString().substring(0, buffer.toString().length() - 1), "utf-8");
        httpPost.setEntity(requestEntity);
        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        CloseableHttpResponse response = httpClient.execute(httpPost);
        HttpEntity entity = response.getEntity();

        String resultString = EntityUtils.toString(entity);
        System.out.println(resultString);
        responseMap = (Map<String, Object>) JSON.parse(resultString);

        return responseMap;
    }

    public static Map<String, Object> requestByJson(String url, String jsonString) throws Exception {

        Map<String, Object> responseMap = null;
        CloseableHttpClient httpClient = new SSLClient();


        HttpPost httpPost = new HttpPost(url);
        StringEntity requestEntity = new StringEntity(jsonString, "utf-8");
        httpPost.setEntity(requestEntity);
        httpPost.setHeader("Content-type", "application/json");

        CloseableHttpResponse response = httpClient.execute(httpPost);
        HttpEntity entity = response.getEntity();

        String resultString = EntityUtils.toString(entity);

        responseMap = (Map<String, Object>) JSON.parse(resultString);

        return responseMap;
    }


    public static void main(String[] args) throws Exception {
        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("appid", "wxb1c61fbe921b4665");
        requestMap.put("secret", "b605b291167597b4c09cb0cbd7c3b6a0");
        requestMap.put("grant_type", "client_credential");




        Map<String, Object> accessTokenByMap = requestByMap("https://api.weixin.qq.com/cgi-bin/token", requestMap);
        if (accessTokenByMap != null && StringUtils.isNotEmpty((String)accessTokenByMap.get("access_token"))) {
            requestMap = new HashMap<>();
            requestMap.put("access_token",(String)accessTokenByMap.get("access_token"));
            requestMap.put("type","jsapi");

            Map<String, Object> ticketMap = HttpUtil.requestByMap("https://api.weixin.qq.com/cgi-bin/ticket/getticket", requestMap);
            if (ticketMap != null && (int) ticketMap.get("errcode") == 0) {
                System.out.println(ticketMap.get("ticket"));
            }
        }
//        try {
//            requestByMap("https://api.weixin.qq.com/cgi-bin/token", requestMap);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

}
