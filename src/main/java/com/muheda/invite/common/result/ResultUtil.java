package com.muheda.invite.common.result;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;


public class ResultUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResultUtil.class);

    /**
     * normal
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> Result<T> genResult(ResultConst resultConst, T data) {
        Result<T> result = Result.newInstance();
        result.setCode(resultConst.getCode());
        result.setData(data);
        result.setMessage(resultConst.getMessage());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("generate rest result:{}", result);
        }
        return result;
    }

    public static <T> Result<T> genResult(int code, String message, T data) {
        Result<T> result = Result.newInstance();
        result.setCode(code);
        result.setData(data);
        result.setMessage(message);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("generate rest result:{}", result);
        }
        return result;
    }

    /**
     * success
     *
     * @param
     * @param <T>
     * @return
     */
    public static <T> Result<T> success() {
        return genResult(ResultConst.SUCCESS, null);
    }

    public static <T> Result<T> success(T data) {
        return genResult(ResultConst.SUCCESS, data);
    }

    public static <T> Result<T> success(Page<T> data) {
        return (Result<T>) genResult(ResultConst.SUCCESS, data);
    }


    /**
     * @param resultConst
     * @param data
     * @param <T>
     * @return
     */
    public static <T> Result<T> error(ResultConst resultConst, T data) {
        return genResult(resultConst, data);
    }

    public static <T> Result<T> error(int code, String message, T data) {
        return genResult(code, message, data);
    }

    public static <T> Result<T> error(int code, String message) {
        return genResult(code, message, null);
    }

    public static <T> Result<T> error(ResultConst resultConst) {
        return genResult(resultConst, null);
    }


}
