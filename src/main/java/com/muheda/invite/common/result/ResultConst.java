package com.muheda.invite.common.result;

public enum ResultConst {

    SUCCESS(0, "SUCCESS"),
    SYSTEM_ERROR(-1, "系统错误，请联系管理员"),
    TOKEN_INVALID(1, "用户授权认证没有通过，请重新登录"),
    TOKEN_EXPIRED(2, "用户授权认证过期，请重新登录"),
    VERIFYCODE_ERROR(3, "验证码错误"),//验证码错误
    SAVE_DATA_ERROR(4, "保存数据失败"),//保存数据失败
//    SERVICE_CLOSE(5, "SERVICE_CLOSE"),//服务关闭
    PARAMETER_ERROR(6, "请求参数错误"),//参数错误
//    SYSTEM_DATA_ERROR(7, "SYSTEM_DATA_ERROR"),//系统数据异常
//    FORMAT_DATE_ERROR(8, "FORMAT_DATE_ERROR"),//时间格式化失败
    HTTP_REQUEST_METHOD_NOT_SUPPORTED(9, "请求方法不支持"),//请求方法不支持
    HTTP_REQUEST_EXCEPTION(10, "请求异常，请联系我们"),
    LOGIN_ERROR(11, "登录系统错误"),//登录系统错误
    REG_ERROR(12, "注册系统错误"),//注册错误
    VERIFYCODE_EXPIRED(13,"验证码过期"),//验证码过期
//    DATE_FORMAT_ERROR(14, "DATE_FORMAT_ERROR"),//时间格式化异常
    STEP_EXCHANGE_ERROR(15, "集步兑换失败，请联系我们"),//集步兑换异常
    NETEASE_VALIDATE_ERROR(16, "图片验证码验证失败"),//网易云盾验证失败
    WX_JS_API_URL_ERROR(17, "微信Api请求地址错误"),//微信 js api 错误
//    ILLEGAL_REQUEST(9999, "ILLEGAL_REQUEST"),//非法请求

    USER_NOT_EXISTS(2001, "用户不存在"),//用户不存在
//    USER_NAME_PASSWORD_ERROR(2002, "USER_NAME_PASSWORD_ERROR"),//用户名或者密码错误
//    USER_NAME_EXISTS(2003, "USER_NAME_EXISTS"),//用户名已经存在
    USER_IS_NOT_NEW_REG_USER(2004, "不是新注册用户"),//不是新注册用户
    USER_ALREADY_EXISTS(2005, "用户已存在"),//用户已存在


    GROUP_NOT_EXISTS(3001, "团队不存在"),//团队不存在
    GROUP_ALREADY_FULL(3002,"团队已经满员"),//团队已经满员
    CREATE_GROUP_SUM_CAN_NOT_MORE_THAN_FIVE(3003, "每天最多创建五个团"),//每天最多创建五个团
    HAVA_NOT_EXCHANGE_INFO(3004,"有未兑换的奖励，请现行兑换"),//有未兑换的奖励
    HAVA_NOT_SUCCESS_GROUP(3005, "有进行中的团，不能创建新团"),//有进行中的团
    PHONE_FORMAT_ERROR(3006, "手机号格式错误"),//手机号检查异常
    NOT_HAVA_NO_EXCHANGE_INFO(3007, "没有未兑换的奖励"),//没有未兑换奖励
    ENCRYPT_ERROR(3008, "系统错误，请联系我们"),//加密错误


    JWT_NOT_FOUND(4031,"用户未登录，请先登录"),
    JWT_CHECK_ERROR(4032,"用户登陆状态验证错误"),
    ;

    private int code;
    private String message;

    ResultConst() {
    }

    ResultConst(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
