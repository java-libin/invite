package com.muheda.invite.common.verificationCode;


import com.muheda.invite.common.util.HttpUtil;
import com.muheda.invite.entity.VerificationCodeLog;
import com.muheda.invite.repository.VerificationCodeLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Administrator on 2017/8/2.
 */
@Component
public class VerificationCodeSendUtil {

    public static String smsUrl;//

    public static String smsAccount;

    public static String smsPassword;

    public static String voiceUrl;//

    public static String voiceAccount;

    public static String voicePassword;

    @Value("${verificationCode.voice.url}")
    public  void setVoiceUrl(String voiceUrl) {
        VerificationCodeSendUtil.voiceUrl = voiceUrl;
    }
    @Value("${verificationCode.voice.account}")
    public  void setVoiceAccount(String voiceAccount) {
        VerificationCodeSendUtil.voiceAccount = voiceAccount;
    }

    @Value("${verificationCode.voice.password}")
    public  void setVoicePassword(String voicePassword) {
        VerificationCodeSendUtil.voicePassword = voicePassword;
    }

    @Value("${verificationCode.sms.url}")
    public void setSmsUrl(String smsUrl) {
        this.smsUrl = smsUrl;
    }

    @Value("${verificationCode.sms.account}")
    public void setSmsAccount(String smsAccount) {
        this.smsAccount = smsAccount;
    }

    @Value("${verificationCode.sms.password}")
    public void setSmsPassword(String smsPassword) {
        this.smsPassword = smsPassword;
    }


//    @Autowired
    private static VerificationCodeLogRepository verificationCodeLogRepository;

//    private static VerificationCodeSendUtil verificationCodeSendUtil;

    @Autowired
    public  void setVerificationCodeLogRepository(VerificationCodeLogRepository verificationCodeLogRepository) {
        VerificationCodeSendUtil.verificationCodeLogRepository = verificationCodeLogRepository;
    }


//    @PostConstruct
//    public void init() {
//        verificationCodeSendUtil=this;
//        verificationCodeSendUtil.verificationCodeLogRepository = this.verificationCodeLogRepository;
//    }

    /**
     * 发送短信
     *
     * @param phone     手机号
     * @param msgContent 短信内容
     * @throws UnsupportedEncodingException
     */
    public static boolean sendSmsMessage(String phone, String msgContent) throws IOException {

        StringBuffer sb = new StringBuffer();
        String requestContent = sb.append("account=").append(smsAccount).append("&pswd=").append(smsPassword).append("&mobile=" + phone).append("&msg=" + URLEncoder.encode(msgContent, "UTF-8")).toString();
        // 请求参数 url 请求地址;mobiles手机号 ;content 短信内容
        System.out.println("requestContent:" + sb.append("account=").append(smsAccount).append("&pswd=").append(smsPassword).append("&mobile=" + phone).append("&msg=" + msgContent).toString());

        String result;

        System.out.println("url:" + smsUrl);
        result = HttpUtil.sendPost(smsUrl, requestContent);

        System.out.println(result);

        VerificationCodeLog verificationCodeLog = new VerificationCodeLog();
        verificationCodeLog.setPhone(phone);
        verificationCodeLog.setMsgContent(msgContent);
        verificationCodeLog.setMessageType(1);


        if (result != null && !"".equals(result)) {
            try {
                String respCode = result.split(",")[1];
                verificationCodeLog.setRespCode(respCode);
                verificationCodeLogRepository.save(verificationCodeLog);
                if ("0".equals(respCode)) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                verificationCodeLog.setException(e.getMessage());
                verificationCodeLogRepository.save(verificationCodeLog);
                return false;
            }
        }
        verificationCodeLogRepository.save(verificationCodeLog);
        return false;
    }

    /**
     * 发送语音短信
     *
     * @param phone
     * @param msgContent
     * @return
     * @throws IOException
     */
    public static boolean sendVoiceMessage(String phone, String msgContent) throws IOException {

        StringBuffer sb = new StringBuffer();
        String requestContent = sb.append("account=").append(voiceAccount).append("&pswd=").append(voicePassword).append("&mobile=" + phone).append("&msg=" + URLEncoder.encode(msgContent, "UTF-8")).toString();
        // 请求参数 url 请求地址;mobiles手机号 ;content 短信内容
        System.out.println("requestContent:" + sb.append("account=").append(voiceAccount).append("&pswd=").append(voicePassword).append("&mobile=" + phone).append("&msg=" + msgContent).toString());

        String result;

        System.out.println("url:" + voiceUrl);
        result = HttpUtil.sendPost(voiceUrl, requestContent);

        System.out.println(result);
        VerificationCodeLog verificationCodeLog = new VerificationCodeLog();
        verificationCodeLog.setPhone(phone);
        verificationCodeLog.setMsgContent(msgContent);
        verificationCodeLog.setMessageType(2);
        if (result != null && !"".equals(result)) {
            try {
                String respCode = result.split(",")[1];
                verificationCodeLog.setRespCode(respCode);
                verificationCodeLogRepository.save(verificationCodeLog);
                if ("0".equals(respCode)) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                verificationCodeLog.setException(e.getMessage());
                verificationCodeLogRepository.save(verificationCodeLog);
                return false;
            }
        }

        verificationCodeLogRepository.save(verificationCodeLog);
        return false;
    }




}
