package com.muheda.invite.common.sdk.utils;

import com.muheda.invite.common.sdk.NECaptchaVerifier;

import java.io.Serializable;



/**
 * Created by sorin on 2017/9/28.
 */
public class WangYiValidate implements Serializable{
	
	private static final long serialVersionUID = -6051014385697316711L;
	private static final String captchaId = "3c56452abec140459bf3ba64163c3fae"; // 验证码id
    private static final String secretId = "c327a811852baa9b209fbedef54966bb"; // 密钥对id
    private static final String secretKey = "1243b6503d5df861b4a946f9430d96fd"; // 密钥对key



    private static final NECaptchaVerifier verifier = new NECaptchaVerifier(captchaId, secretId, secretKey);

    public static boolean validate(String validate){
        return verifier.verify(validate, null); // 发起二次校验
    }
}
