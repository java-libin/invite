package com.muheda.invite.common.exception;


import com.muheda.invite.common.result.ResultConst;

/**
 * Created by libin on 2018/1/6.
 */
public class BaseException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -2319582498945366392L;

    private ResultConst resultConst;

    public BaseException() {
        super();
    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable e) {
        super(message, e);
    }

    public BaseException(int code, String message) {
        this.message = message;
        this.code = code;
    }


    public BaseException(ResultConst resultConst) {
        super(resultConst.getCode() + "");
        this.resultConst = resultConst;
    }

    public ResultConst getResultConst() {
        return resultConst;
    }

    public int code;
    private String message;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setResultConst(ResultConst resultConst) {
        this.resultConst = resultConst;
    }
}