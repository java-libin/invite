package com.muheda.invite.common.exception;

import com.muheda.invite.common.result.Result;
import com.muheda.invite.common.result.ResultConst;
import com.muheda.invite.common.result.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result jsonErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        Result result = null;
        if (e instanceof HttpRequestMethodNotSupportedException) {
            ResultUtil.error(ResultConst.HTTP_REQUEST_METHOD_NOT_SUPPORTED);
        } else {
            result = ResultUtil.error(ResultConst.SYSTEM_ERROR);
        }
        logger.error("GlobalExceptionHandler：req url:{} params:{}", req.getRequestURI(), "");
        return result;
    }
}