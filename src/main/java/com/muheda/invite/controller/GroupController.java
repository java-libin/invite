package com.muheda.invite.controller;

import com.muheda.invite.common.exception.BaseException;
import com.muheda.invite.common.result.Result;
import com.muheda.invite.entity.ExchangeInfo;
import com.muheda.invite.entity.Group;
import com.muheda.invite.service.GroupService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by libin on 2018/7/14.
 */
@RestController
@RequestMapping("group")
public class GroupController extends BaseController {

    @Autowired
    private GroupService groupService;

    /**
     * 创建团队
     *
     * @return
     */
    @GetMapping("add")
    public Result<Map<String, Object>> addGroup() {

        String userUuid = getUserUuidFromToken();//user  uuid

        if (StringUtils.isAnyEmpty(userUuid)) {
            return writeParameterError();
        }

        try {
            Group group = groupService.addGroup(userUuid);
            return writeSuccess("group",group);
        } catch (BaseException e) {
            e.printStackTrace();
            return writeError(e);
        } catch (Exception e) {
            e.printStackTrace();
            return writeSystemError();
        }
    }


    /**
     * 加入团队
     * @param requestBody
     * @return
     */
    @PostMapping("join")
    public Result<Map<String, Object>> joinGroup(@RequestBody Map<String, String> requestBody) {

        String userUuid = getUserUuidFromToken();//user uuid
        String groupUuid = requestBody.get("groupUuid");// 组团uuid



        if (StringUtils.isAnyEmpty(userUuid,groupUuid)) {
            return writeParameterError();
        }


        try {
            Group group =groupService.joinGroup(userUuid, groupUuid);
            return writeSuccess("group",group);
        } catch (BaseException e) {
            e.printStackTrace();
            return writeError(e);
        } catch (Exception e) {
            e.printStackTrace();
            return writeSystemError();
        }
    }


    /**
     * 用户归属团队
     * @return
     */
    @GetMapping("user/belong")
    public Result<Map<String, Object>> getUserNotExchange() {

        String userUuid = getUserUuidFromToken();//user uuid

        if (StringUtils.isAnyEmpty(userUuid)) {
            return writeParameterError();
        }
        try {
            Group group = groupService.gerUserBelongGroup(userUuid);
            return writeSuccess("group",group);
//        } catch (BaseException e) {
//            e.printStackTrace();
//            return writeError(e);
        } catch (Exception e) {
            e.printStackTrace();
            return writeSystemError();
        }
    }


}
