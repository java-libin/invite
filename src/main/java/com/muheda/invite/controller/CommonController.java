package com.muheda.invite.controller;

import com.muheda.invite.common.exception.BaseException;
import com.muheda.invite.common.result.Result;
import com.muheda.invite.common.result.ResultConst;
import com.muheda.invite.common.util.SHA1Util;
import com.muheda.invite.common.util.WxUtil;
import com.muheda.invite.core.redis.RedisUtil;
import com.muheda.invite.service.ActivityService;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by libin on 2018/7/18.
 */
@RestController
@RequestMapping("common")
public class CommonController extends BaseController{

    @Autowired
    private ActivityService activityService;

    @Autowired
    private RedisUtil redisUtil;

    @Value("${wx.access-token.redis-key}")
    private String accessTokenRedisKey;

    @Value("${wx.ticket.redis-key}")
    private String ticketRedisKey;

    @Value("${wx.appid}")
    private String appid;

    @GetMapping("alreadyPartitionSum")
    public Result<Map<String, Object>> partitionSum() {

        try {
            Integer alreadyPartitionSum = activityService.getAlreadyPartitionSum();
            return writeSuccess("alreadyPartitionSum",alreadyPartitionSum);
        } catch (BaseException e) {
            return writeError(e);
        } catch (Exception e) {
            e.printStackTrace();
            return writeSystemError();
        }
    }

    @PostMapping("wxJsApi")
    public Result<Map<String, Object>> wxJsApi(@RequestBody Map<String, String> requestBody) {

        String url = requestBody.get("url");

        if (StringUtils.isAnyEmpty(url)) {
            return writeParameterError();
        }

        try {
            url = new java.net.URI(url).getPath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return writeError(ResultConst.WX_JS_API_URL_ERROR);
        }

        System.out.println(redisUtil.get(ticketRedisKey + appid));

        String jsapiTicket = String.valueOf(redisUtil.get(ticketRedisKey + appid));

        if (StringUtils.isEmpty(jsapiTicket) || "null".equals(jsapiTicket) ||"".equals(jsapiTicket)) {
            jsapiTicket =  WxUtil.addWxInfoToRedis();
        }

        Long timestamp = System.currentTimeMillis();
        String noncestr = String.valueOf(RandomUtils.nextInt(1000000, 9999999));


        String needSignString = "jsapi_ticket=" + jsapiTicket + "&noncestr=" + noncestr + "&timestamp=" + timestamp + "&url=" + url;
        String signature = SHA1Util.encode(needSignString);


        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("timestamp", timestamp);
        resultMap.put("appid", appid);
        resultMap.put("noncestr", noncestr);
        resultMap.put("signature", signature);
        return writeSuccess(resultMap);
    }


}
