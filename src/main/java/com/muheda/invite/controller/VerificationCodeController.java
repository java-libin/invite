package com.muheda.invite.controller;

import com.muheda.invite.common.exception.BaseException;
import com.muheda.invite.common.result.Result;
import com.muheda.invite.common.result.ResultConst;
import com.muheda.invite.common.sdk.utils.WangYiValidate;
import com.muheda.invite.common.util.PhoneFormatCheckUtils;
import com.muheda.invite.service.MessageService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;

/**
 * Created by libin on 2018/7/14.
 */
@RestController
@RequestMapping("verificationCode")
public class VerificationCodeController extends BaseController {


    @Autowired
    private MessageService messageService;

    /**
     * 发送短信验证码
     *
     * @param requestBody
     * @return
     */
    @PostMapping("sms")
    public Result<Map<String, Object>> sendSms(@RequestBody Map<String, String> requestBody) {
        String phone = requestBody.get("phone");


        String validate = requestBody.get("validate");


        if (StringUtils.isAnyEmpty(phone,validate)) {
//        if (StringUtils.isAnyEmpty(phone)) {
            return writeParameterError();
        }

        if (!PhoneFormatCheckUtils.isChinaPhoneLegal(phone)) {
            return writeError(ResultConst.PHONE_FORMAT_ERROR);
        }


        if (!WangYiValidate.validate(validate)) {
            return writeError(ResultConst.NETEASE_VALIDATE_ERROR);
        }

        try {
            boolean result = messageService.sendVerificationCodeMessage(phone, 1);
            return writeSuccess("result", result);
        } catch (BaseException e) {
            e.printStackTrace();
            return writeError(e);
        } catch (IOException e) {
            e.printStackTrace();
            return writeSystemError();
        } catch (Exception e) {
            e.printStackTrace();
            return writeSystemError();
        }

    }

    /**
     * 发送语音验证码
     *
     * @param requestBody
     * @return
     * @throws IOException
     */
    @PostMapping("voice")
    public Result<Map<String, Object>> sendVoice(@RequestBody Map<String, String> requestBody) {
        String phone = requestBody.get("phone");


        String validate = requestBody.get("validate");


        if (StringUtils.isAnyEmpty(phone,validate)) {
//        if (StringUtils.isAnyEmpty(phone)) {
            return writeParameterError();
        }

        if (!PhoneFormatCheckUtils.isChinaPhoneLegal(phone)) {
            return writeError(ResultConst.PHONE_FORMAT_ERROR);
        }


        if (!WangYiValidate.validate(validate)) {
            return writeError(ResultConst.NETEASE_VALIDATE_ERROR);
        }

        try {
            boolean result = messageService.sendVerificationCodeMessage(phone, 2);
            return writeSuccess("result", result);
        } catch (BaseException e) {
            e.printStackTrace();
            return writeError(e);
        } catch (IOException e) {
            e.printStackTrace();
            return writeSystemError();
        } catch (Exception e) {
            e.printStackTrace();
            return writeSystemError();
        }
    }


    @PostMapping("netease")
    public Result<Map<String, Object>> neteaseTest(@RequestBody Map<String, String> requestBody) {
        String validate = requestBody.get("validate");
        try {
            boolean result = WangYiValidate.validate(validate);
            return writeSuccess("result", result);
        } catch (Exception e) {
            e.printStackTrace();
            return writeSystemError();
        }
    }
}
