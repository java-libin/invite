package com.muheda.invite.controller;

import com.muheda.invite.common.exception.BaseException;
import com.muheda.invite.common.result.Result;
import com.muheda.invite.common.result.ResultConst;
import com.muheda.invite.common.sdk.utils.WangYiValidate;
import com.muheda.invite.common.util.PhoneFormatCheckUtils;
import com.muheda.invite.config.properties.JwtProperties;
import com.muheda.invite.entity.User;
import com.muheda.invite.security.JwtTokenUtil;

import com.muheda.invite.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by libin on 2018/7/11.
 */
@RestController
@RequestMapping("user")
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    @PostMapping("login")
    public Result<Map<String,Object>> login(@RequestBody Map<String, String> requestBody) {

        String phone = requestBody.get("phone");
        String password = requestBody.get("password");



        if (StringUtils.isAnyEmpty(phone, password)) {
            return writeParameterError();
        }


        if (!PhoneFormatCheckUtils.isChinaPhoneLegal(phone)) {
            return writeError(ResultConst.PHONE_FORMAT_ERROR);
        }

        try {
            User user = userService.userLogin(phone, password);
            String token = jwtTokenUtil.generateToken(user.getUserUuid(), user.getPhone(), jwtTokenUtil.getRandomKey());
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("user", user);
            resultMap.put("Authorization", token);
            return writeSuccess(resultMap);
        } catch (BaseException e) {
            e.printStackTrace();
            return writeError(e);
        } catch (Exception e) {
            e.printStackTrace();
            return writeSystemError();
        }
    }



    @PostMapping("register")
    public Result<Map<String,Object>> reg(@RequestBody Map<String, String> requestBody) {

        String phone = requestBody.get("phone");
        String smsCode = requestBody.get("smsCode");
        String inviterUserUuid = requestBody.get("inviterUserUuid");

        if (StringUtils.isAnyEmpty(phone, smsCode)) {
            return writeParameterError();
        }


        if (!PhoneFormatCheckUtils.isChinaPhoneLegal(phone)) {
            return writeError(ResultConst.PHONE_FORMAT_ERROR);
        }


        try {
            User user = userService.userReg(phone, smsCode, inviterUserUuid);
            String token = jwtTokenUtil.generateToken(user.getUserUuid(),user.getPhone(),jwtTokenUtil.getRandomKey());
//            response.addHeader("Authorization",token);

            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("user", user);
            resultMap.put("Authorization", token);

            return writeSuccess(resultMap);
        } catch (BaseException e) {
            e.printStackTrace();
            return writeError(e);
        } catch (Exception e) {
            e.printStackTrace();
            return writeSystemError();
        }
    }

}
