package com.muheda.invite.controller;

import com.muheda.invite.common.exception.BaseException;
import com.muheda.invite.common.result.Result;
import com.muheda.invite.common.result.ResultConst;
import com.muheda.invite.common.sdk.utils.WangYiValidate;
import com.muheda.invite.common.util.PhoneFormatCheckUtils;
import com.muheda.invite.service.StepService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by libin on 2018/7/19.
 */
@RestController
@RequestMapping("step")
public class StepController extends BaseController {

    @Autowired
    private StepService stepService;


    @PostMapping("exchange")
    public Result<Map<String, Object>> exchange(@RequestBody Map<String, String> requestBody) {


        String phone = requestBody.get("phone");

        //TODO prod need validate check

        String validate = requestBody.get("validate");
        if (StringUtils.isAnyEmpty(phone,validate)) {
//        if (StringUtils.isAnyEmpty(phone)) {
            return writeParameterError();
        }

        if (!PhoneFormatCheckUtils.isChinaPhoneLegal(phone)) {
            return writeError(ResultConst.PHONE_FORMAT_ERROR);
        }


        if (!WangYiValidate.validate(validate)) {
            return writeError(ResultConst.NETEASE_VALIDATE_ERROR);
        }
//
        try {
            stepService.exchange(phone);
            return writeSuccess();
        } catch (BaseException e) {
            e.printStackTrace();
            return writeError(e);
        } catch (Exception e) {
            e.printStackTrace();
            return writeSystemError();
        }

    }

}

