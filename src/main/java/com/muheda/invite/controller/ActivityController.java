package com.muheda.invite.controller;

import com.muheda.invite.common.exception.BaseException;
import com.muheda.invite.common.result.Result;
import com.muheda.invite.common.result.ResultConst;
import com.muheda.invite.entity.ExchangeInfo;
import com.muheda.invite.entity.TotalRewardVo;
import com.muheda.invite.entity.User;
import com.muheda.invite.service.ActivityService;
import com.muheda.invite.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by libin on 2018/7/14.
 */
@RestController
@RequestMapping("activity")
public class ActivityController extends BaseController {

    @Autowired
    private ActivityService activityService;


    @Autowired
    private UserService userService;

    @GetMapping("sort")
    public Result<Map<String, Object>> sort() {

        try {
            List<User> list = activityService.getExchangeSort();
            return writeList(list);
        } catch (BaseException e) {
            e.printStackTrace();
            return writeError(e);
        } catch (Exception e) {
            e.printStackTrace();
            return writeSystemError();
        }


    }


    /**
     * 红包记录
     *
     * @param requestBody
     * @return
     */
    @PostMapping("detail")
    public Result<Map<String, Object>> detail(@RequestBody Map<String, String> requestBody) {
        String userUuid = getUserUuidFromToken();

        if (StringUtils.isEmpty(userUuid)) {
            return writeParameterError();
        }



        try {

            User user = userService.findByUserUuid(userUuid);

            if (user == null) {
                return writeError(ResultConst.USER_NOT_EXISTS);
            }

            Map resultMap = new HashMap();

            TotalRewardVo totalRewardVo = new TotalRewardVo();
            totalRewardVo.setActivityPoint(user.getActivityPoint()*2);
            totalRewardVo.setActivityPointCoupon(user.getActivityPointCoupon());
//            BeanUtils.copyProperties(user,totalRewardVo);
            Pageable pageable = createPageable(requestBody);
            Page<ExchangeInfo> page = activityService.getUserActivityDetail(userUuid, pageable);
            resultMap.put("totalReward", totalRewardVo);
            resultMap.put("page", page);

            return writeSuccess(resultMap);
        } catch (BaseException e) {
            return writeError(e);
        } catch (Exception e) {
            e.printStackTrace();
            return writeSystemError();
        }

    }

    /**
     * 拆红包
     *
     * @param requestBody
     * @return
     */
    @PostMapping("exchange")
    public Result<Map<String, Object>> exchange(@RequestBody Map<String, String> requestBody) {
        String userUuid = getUserUuidFromToken();
        String groupUuid = requestBody.get("groupUuid");

        if (StringUtils.isAnyEmpty(userUuid, groupUuid)) {
            return writeParameterError();
        }

        try {
            activityService.exchange(userUuid, groupUuid);
            return writeSuccess();
        } catch (BaseException e) {
            return writeError(e);
        } catch (Exception e) {
            e.printStackTrace();
            return writeSystemError();
        }
    }



}

