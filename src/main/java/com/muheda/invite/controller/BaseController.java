package com.muheda.invite.controller;

import com.alibaba.druid.util.StringUtils;
import com.muheda.invite.common.exception.BaseException;
import com.muheda.invite.common.result.Result;
import com.muheda.invite.common.result.ResultConst;
import com.muheda.invite.common.result.ResultUtil;
import com.muheda.invite.config.properties.JwtProperties;
import com.muheda.invite.security.JwtTokenUtil;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by libin on 2018/1/23.
 */
public abstract class BaseController {
    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected HttpSession session;


    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtProperties jwtProperties;

    public static ResultConst getResultConstByCode(int code) {
        for (ResultConst resultConst : ResultConst.values()) {
            if (StringUtils.equals(code + "", resultConst.getCode() + "")) {
                return resultConst;
            }
        }
        return ResultConst.SYSTEM_ERROR;
    }


    public String getUsernameFromToken() {
        String token = request.getHeader(jwtProperties.getHeader());//header方式
        return jwtTokenUtil.getUsernameFromToken(token);
    }

    public String getUserUuidFromToken() {
        String token = request.getHeader(jwtProperties.getHeader());//header方式
        return (String) jwtTokenUtil.getClaimFromToken(token).get("userUuid");
    }

//    public static void startPage(Map<String, String> map) {
//        int pageNum, pageSize;
//        if (map == null || map.size() == 0) {
//            pageNum = 1;
//            pageSize = 10;
//        } else {
//            try {
//                pageNum = Integer.parseInt(map.get("pageNum"));
//            } catch (Exception e) {
//                pageNum = 1;
//            }
//            try {
//                pageSize = Integer.parseInt(map.get("pageSize"));
//            } catch (Exception e) {
//                pageSize = 10;
//            }
//        }
//        PageHelper.startPage(pageNum, pageSize);
//    }

    /**
     * 程序运行成功后返回的结果
     *
     * @return
     */
//    protected <O extends Object> Result<O> writeSuccess(O o){
//        return ResultUtil.success(o);
//    }

//    public static void startPageOrderBy(Map<String, String> map, String orderBy) {
//        int pageNum, pageSize;
//        if (map == null || map.size() == 0) {
//            pageNum = 1;
//            pageSize = 10;
//        } else {
//            try {
//                pageNum = Integer.parseInt(map.get("pageNum"));
//            } catch (Exception e) {
//                pageNum = 1;
//            }
//            try {
//                pageSize = Integer.parseInt(map.get("pageSize"));
//            } catch (Exception e) {
//                pageSize = 10;
//            }
//        }
//        PageHelper.startPage(pageNum, pageSize, orderBy);
//    }

//    public static <T> PageInfo bulidPageInfo(List<T> list) {
//        return new PageInfo(list);
//    }
    public static Map<String, Object> builtResultMap(String key, Object obj) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put(key, obj);
        return resultMap;
    }

    /**
     * 程序运行成功后返回的结果
     *
     * @return
     */
    protected <O extends Object> Result<O> writeSuccess() {
        return ResultUtil.success();
    }

    protected <O extends Object> Result<O> writeSuccess(String key, Object obj) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put(key, obj);
        return ResultUtil.success((O) resultMap);
    }

    protected <O extends Object> Result<O> writePageInfo(Page page) {
        Map resultMap = new HashMap<>();
        resultMap.put("page", page);
        return ResultUtil.success((O) resultMap);
    }

//    protected <O extends Object> Result<O> writePageInfo(List list) {
//        Map resultMap = new HashMap<>();
//        resultMap.put("pageInfo", bulidPageInfo(list));
//        return ResultUtil.success((O) resultMap);
//    }

    protected <O extends Object> Result<O> writeList(List list) {
        Map resultMap = new HashMap<>();
        resultMap.put("list", list);
        return ResultUtil.success((O) resultMap);
    }

    /**
     * 程序运行失败后服务器返回的结果
     *
     * @return
     */
    protected <O extends Object> Result<O> writeSystemError() {
        return ResultUtil.error(ResultConst.SYSTEM_ERROR);
    }


    protected Pageable createPageable(Map<String, String> map) {
        int pageNum, pageSize;
        if (map == null || map.size() == 0) {
            pageNum = 0;
            pageSize = 10;
        } else {
            try {
                pageNum = Integer.parseInt(map.get("pageNum"));
            } catch (Exception e) {
                pageNum = 0;
            }
            try {
                pageSize = Integer.parseInt(map.get("pageSize"));
            } catch (Exception e) {
                pageSize = 10;
            }
        }
        return new PageRequest(pageNum, pageSize);
    }

    /**
     * 程序运行成功后返回的结果
     * o 为程序业务逻辑成功运行后返回的对象
     *
     * @return
     */
    protected <O extends Object> Result<O> writeSuccess(O o) {
        return ResultUtil.success(o);
    }

    /**
     * 程序运行失败后服务器返回的结果
     *
     * @return
     */
    protected <O extends Object> Result<O> writeError(ResultConst resultConst) {
        return ResultUtil.error(resultConst);
    }

    protected <O extends Object> Result<O> writeError(ResultConst resultConst, O o) {
        return ResultUtil.error(resultConst, o);
    }

    protected <O extends Object> Result<O> writeResultConst(ResultConst resultConst) {
        if (resultConst.equals(ResultConst.SUCCESS)) {
            return ResultUtil.success();
        } else {
            return ResultUtil.error(resultConst);
        }
    }

    protected <O extends Object> Result<O> writeError(int code, String message) {
        return ResultUtil.error(getResultConstByCode(code));
    }

    protected <O extends Object> Result<O> writeError(BaseException e) {
        if (e.getResultConst() == null) {
            return ResultUtil.error(e.getCode(), e.getMessage());
        }
        return ResultUtil.error(e.getResultConst());
    }

    protected <O extends Object> Result<O> writeParameterError() {
        return ResultUtil.error(ResultConst.PARAMETER_ERROR);
    }

    @ModelAttribute
    public void setReqAndRes(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * * 获取用户真实IP地址，不使用request.getRemoteAddr();的原因是有可能用户使用了代理软件方式避免真实IP地址, * *
     * 可是，如果通过了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP值，究竟哪个才是真正的用户端的真实IP呢？ *
     * 答案是取X-Forwarded-For中第一个非unknown的有效IP字符串。 * *
     * 如：X-Forwarded-For：192.168.1.110, 192.168.1.120, 192.168.1.130, *
     * 192.168.1.100 * * 用户真实IP为： 192.168.1.110 * * @param request * @return
     */
    protected String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }


}




