package com.muheda.invite.service;

import com.muheda.invite.entity.ExchangeInfo;
import com.muheda.invite.entity.TotalRewardVo;
import com.muheda.invite.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by libin on 2018/7/14.
 */
public interface ActivityService {
    Page<ExchangeInfo> getUserActivityDetail(String userUuid, Pageable pageable) throws Exception;

    List<User> getExchangeSort()  throws Exception;

    void exchange(String userUuid, String groupUuid) throws Exception;

    Integer getAlreadyPartitionSum() throws Exception;

}
