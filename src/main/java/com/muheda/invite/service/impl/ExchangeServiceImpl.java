package com.muheda.invite.service.impl;

import com.muheda.invite.entity.ExchangeInfo;
import com.muheda.invite.repository.ExchangeRepository;
import com.muheda.invite.service.ExchangeService;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by libin on 2018/7/14.
 */
@Service
public class ExchangeServiceImpl implements ExchangeService {

    @Autowired
    private ExchangeRepository exchangeRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void groupSuccess(int groupId, String groupUuid, String organizerUserUuid, List<String> groupMemberUuids) throws Exception {
        //先创建团长记录
        ExchangeInfo exchangeInfo = new ExchangeInfo();
        exchangeInfo.setExchangeType(1);
        exchangeInfo.setActivityPointCouponSum(1);
        exchangeInfo.setActivityPointSum(1);
        exchangeInfo.setUserUuid(organizerUserUuid);
        exchangeInfo.setGroupId(groupId);
        exchangeInfo.setGroupUuid(groupUuid);
        exchangeInfo.setUserType(2);
        exchangeInfo.setRealUserData(true);
        exchangeRepository.save(exchangeInfo);


        //点券3个 一人一个
        for (String groupMemberUuid : groupMemberUuids) {
            if (groupMemberUuid==null) continue;

            exchangeInfo = new ExchangeInfo();
            exchangeInfo.setGroupId(groupId);
            exchangeInfo.setExchangeType(2);
            exchangeInfo.setActivityPointCouponSum(1);
            exchangeInfo.setUserUuid(groupMemberUuid);
            if (groupMemberUuid.equalsIgnoreCase(organizerUserUuid)) {
                exchangeInfo.setUserType(2);
            } else {
                exchangeInfo.setUserType(1);
            }
            exchangeInfo.setGroupId(groupId);
            exchangeInfo.setGroupUuid(groupUuid);
            exchangeInfo.setRealUserData(true);
            exchangeRepository.save(exchangeInfo);
        }


        /**
         *
         * 先3，7计算比例 百分之三十几率发放给一个人，百分之七十几率发放给两个人
         *
         * 确定好3，7分配之后，计算分给哪个人
         *
         */

        //生成随机比例分配
        int random2;
        int random1 = RandomUtils.nextInt(1, 10);
        if (random1 < 4) {
             random2= RandomUtils.nextInt(1, 3);
        } else {
             random2 = RandomUtils.nextInt(4, 6);
        }

        switch (random2) {
            case 1://全给第一个人
                toOnePerson(groupId,groupMemberUuids.get(0));
                break;
            case 2://全部给第二个人
                toOnePerson(groupId,groupMemberUuids.get(1));
                break;
            case 3://全部给第三个人
                toOnePerson(groupId,groupMemberUuids.get(2));
                break;
            case 4://给 1，2 两个人
                toTwoPerson(groupId,groupMemberUuids.get(0), groupMemberUuids.get(1));
                break;
            case 5://给1，3 两个人
                toTwoPerson(groupId,groupMemberUuids.get(0), groupMemberUuids.get(2));
                break;
            case 6://给2，3两个人
                toTwoPerson(groupId,groupMemberUuids.get(1), groupMemberUuids.get(2));
                break;
            default:
                break;
        }

    }

    @Transactional(propagation = Propagation.REQUIRED)
    private void toTwoPerson(int groupId,String userUuid1, String userUuid2)  throws Exception{
        ExchangeInfo exchangeInfo = exchangeRepository.findByGroupIdAndUserUuidAndExchangeType(groupId, userUuid1,2);
        if (exchangeInfo != null) {
            exchangeInfo.setActivityPointSum(1);
            exchangeRepository.save(exchangeInfo);
        }
        ExchangeInfo exchangeInfo1 = exchangeRepository.findByGroupIdAndUserUuidAndExchangeType(groupId, userUuid2,2);
        if (exchangeInfo1 != null) {
            exchangeInfo1.setActivityPointSum(1);
            exchangeRepository.save(exchangeInfo);
        }

    }

    @Transactional(propagation = Propagation.REQUIRED)
    private void toOnePerson(int groupId,String userUuid) throws Exception {
        ExchangeInfo exchangeInfo = exchangeRepository.findByGroupIdAndUserUuidAndExchangeType(groupId, userUuid,2);
        if (exchangeInfo != null) {
            exchangeInfo.setActivityPointSum(2);
            exchangeRepository.save(exchangeInfo);
        }
    }


}
