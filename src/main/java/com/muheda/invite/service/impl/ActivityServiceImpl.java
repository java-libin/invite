package com.muheda.invite.service.impl;

import com.muheda.invite.common.exception.BaseException;
import com.muheda.invite.common.result.ResultConst;
import com.muheda.invite.common.util.EncryptDecrypt;
import com.muheda.invite.common.util.HttpUtil;
import com.muheda.invite.common.util.MD5Util;
import com.muheda.invite.entity.ExchangeInfo;
import com.muheda.invite.entity.Partition;
import com.muheda.invite.entity.User;
import com.muheda.invite.repository.ExchangeRepository;
import com.muheda.invite.repository.GroupRepository;
import com.muheda.invite.repository.PartitionRepository;
import com.muheda.invite.repository.UserRepository;
import com.muheda.invite.service.ActivityService;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by libin on 2018/7/14.
 */
@Service
public class ActivityServiceImpl implements ActivityService {


    @Autowired
    private ExchangeRepository exchangeRepository;


    @Autowired
    private UserRepository userRepository;


    @Autowired
    private PartitionRepository partitionRepository;


    @Autowired
    private GroupRepository groupRepository;


    @Value("${muheda.request.url.grant}")
    private String grantUrl;
    @Value("${muheda.security.encodeKey}")
    private String encodeKey;
    @Value("${muheda.security.signKey}")
    private String signKey;

    @Override
    public Page<ExchangeInfo> getUserActivityDetail(String userUuid, Pageable pageable) throws Exception {
        Page<ExchangeInfo> exchangeInfos = exchangeRepository.findAllByExchangeStatusAndUserUuid(1, userUuid, pageable);
        return exchangeInfos;
    }


    @Override
    public List<User> getExchangeSort() throws Exception {
        List<User> list = userRepository.findTop20ByOrderByActivityPointDesc();
        return list;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void exchange(String userUuid, String groupUuid) throws Exception {
        List<ExchangeInfo> exchangeInfos = exchangeRepository.findByGroupUuidAndUserUuidAndExchangeStatus(groupUuid, userUuid, 0);
        if (exchangeInfos == null || exchangeInfos.size() == 0) {
            throw new BaseException(ResultConst.NOT_HAVA_NO_EXCHANGE_INFO);
        }

        int mark = 0;

        if (exchangeInfos.get(0).getUserType() == 1) {
            mark = 101;
        } else {
            mark = 102;
        }
        int goldNumber = 0;//点数量（非必填）
        int welfareNumber = 0;//点券数量（非必填）


        for (ExchangeInfo exchangeInfo : exchangeInfos) {
            if (exchangeInfo.getActivityPointSum() != null) {
                goldNumber += exchangeInfo.getActivityPointSum();
            }
            if (exchangeInfo.getActivityPointCouponSum() != null) {
                welfareNumber += exchangeInfo.getActivityPointCouponSum();
            }
        }


        // http request muheda api

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("mark", mark + "");
        paramMap.put("goldNumber", goldNumber + "");
        paramMap.put("welfareNumber", welfareNumber + "");
        paramMap.put("uuid", userUuid);


        String params = null;
        Map<String, String> retMap = EncryptDecrypt.encrypt(paramMap, encodeKey, signKey);
        if (retMap != null && "000".equals(retMap.get("resultCode"))) {
            params = retMap.get("resultData");
        } else {
            throw new BaseException(ResultConst.ENCRYPT_ERROR);
        }
        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("params", params);


        Map<String, Object> resultMap = HttpUtil.requestByMap(grantUrl, requestMap);
        if ((Boolean) resultMap.get("success") && "200".equals((String) resultMap.get("code"))) {
            for (ExchangeInfo exchangeInfo : exchangeInfos) {
                if (exchangeInfo == null) continue;
                exchangeInfo.setExchangeStatus(1);
                exchangeInfo.setExchangeDate(new Date());

            }
            exchangeRepository.save(exchangeInfos);

            int notExchangeSum = exchangeRepository.countByGroupUuidAndExchangeStatus(groupUuid, 0);

            if (notExchangeSum == 0) {
                groupRepository.updateGroupStatusByGroupUuid(3, groupUuid);
            }


            User user = userRepository.findByUserUuid(userUuid);
            user.setActivityPoint(user.getActivityPoint() + goldNumber);
            user.setActivityPointCoupon(user.getActivityPointCoupon() + welfareNumber);
            userRepository.save(user);

        } else {
            throw new BaseException(Integer.parseInt(String.valueOf(resultMap.get("code"))), (String) resultMap.get("message"));
        }

        int partitionSum = RandomUtils.nextInt(50, 100);
        partitionRepository.updatePartition(partitionSum);
    }


    @Override
    public Integer getAlreadyPartitionSum() throws Exception {
        Partition partition = partitionRepository.findById(1);
        if (partition == null) {
            return 0;
        } else {
            return partition.getPartitionSum();
        }

    }
}
