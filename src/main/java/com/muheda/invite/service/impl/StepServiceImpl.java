package com.muheda.invite.service.impl;

import com.muheda.invite.common.exception.BaseException;
import com.muheda.invite.common.result.ResultConst;
import com.muheda.invite.common.util.EncryptDecrypt;
import com.muheda.invite.common.util.HttpUtil;
import com.muheda.invite.common.util.MD5Util;
import com.muheda.invite.entity.User;
import com.muheda.invite.service.StepService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by libin on 2018/7/19.
 */
@Service
public class StepServiceImpl implements StepService {
    @Value("${muheda.security.encodeKey}")
    private String encodeKey;
    @Value("${muheda.security.signKey}")
    private String signKey;


    @Value("${muheda.request.url.step}")
    private String stepUrl;
    @Override
    public void exchange(String phone) throws Exception {


        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("mobile", phone);

        String params = null;
        Map<String, String> retMap = EncryptDecrypt.encrypt(paramMap, encodeKey, signKey);
        if (retMap != null && "000".equals(retMap.get("resultCode"))) {
            params = retMap.get("resultData");
        } else {
            throw new BaseException(ResultConst.ENCRYPT_ERROR);
        }



        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("params", params);


        Map<String, Object> resultMap = HttpUtil.requestByMap(stepUrl, requestMap);

        if (resultMap == null || resultMap.size() == 0) {
            throw new BaseException(ResultConst.STEP_EXCHANGE_ERROR);
        }

        if (!(Boolean) resultMap.get("success") || !"200".equals((String)resultMap.get("code"))) {
            throw new BaseException(Integer.parseInt(String.valueOf(resultMap.get("code"))), (String) resultMap.get("message"));
        }
    }
}
