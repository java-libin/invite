package com.muheda.invite.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.muheda.invite.common.exception.BaseException;
import com.muheda.invite.common.result.ResultConst;
import com.muheda.invite.common.util.*;
import com.muheda.invite.core.redis.RedisUtil;
import com.muheda.invite.entity.User;
import com.muheda.invite.repository.UserRepository;
import com.muheda.invite.service.MessageService;
import com.muheda.invite.service.UserService;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.muheda.invite.common.util.EncodeDecode.decodeValue;

/**
 * Created by libin on 2018/7/11.
 */
@Service
public class UserServiceImpl implements UserService {


    private Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private UserRepository userRepository;

    @Value("${muheda.request.url.login}")
    private String loginUrl;

    @Value("${muheda.request.url.reg}")
    private String regUrl;

    @Value("${muheda.security.encodeKey}")
    private String encodeKey;
    @Value("${muheda.security.signKey}")
    private String signKey;


    @Autowired
    private MessageService messageService;

    @Autowired
    private RedisUtil redisUtil;

    @Value("${verificationCode.redis-key}")
    private String inviteVerificationCodePrefix;

    @Override
    public Page<User> findAll(Pageable page) throws Exception {
        return userRepository.findAll(page);
    }

    @Override
    public User findByUserUuid(String userUuid) throws Exception {
        User user = userRepository.findByUserUuid(userUuid);
        if (user == null) {
            throw new BaseException(ResultConst.USER_NOT_EXISTS);
        }
        return user;
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public User userLogin(String phone, String password) throws Exception {

        // 请求参数 url 请求地址;mobile手机号 ;password 密码 md5加密
        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("mobile", phone);
        requestMap.put("password", MD5Util.encrypt(password));

        Map<String, Object> resultMap = HttpUtil.requestByMap(loginUrl, requestMap);

        if (resultMap == null || resultMap.size() == 0) {
            throw new BaseException(ResultConst.LOGIN_ERROR);
        }

        if ((Boolean) resultMap.get("success")  && "200".equals((String)resultMap.get("code"))) {
            String userUuid = (String) ((Map) resultMap.get("data")).get("uuid");
            User user = userRepository.findByUserUuid(userUuid);
            if (user == null) {
                user = new User();
                user.setNewRegUser(false);
                user.setPhone(phone);
                user.setActivityPoint(0);
                user.setActivityPointCoupon(0);
                user.setUserUuid(userUuid);
                user.setActivityRegUser(false);
                user = userRepository.saveAndFlush(user);

            }
            return user;
        } else {
            throw new BaseException(Integer.parseInt(String.valueOf(resultMap.get("code"))), (String) resultMap.get("message"));
        }
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public User userReg(String phone, String smsCode, String inviterUserUuid) throws Exception {


        User user = userRepository.findByPhone(phone);
        if (user == null) {

            //redis 读取验证码

            String verificationCode = String.valueOf(redisUtil.get(inviteVerificationCodePrefix + phone));
            if (StringUtils.isEmpty(verificationCode)) {
                throw new BaseException(ResultConst.VERIFYCODE_EXPIRED);
            }

            if (!smsCode.equals(verificationCode)) {
                throw new BaseException(ResultConst.VERIFYCODE_ERROR);
            }


            String password = String.valueOf(RandomUtils.nextInt(1000, 9999));

            Map<String, String> paramMap = new HashMap<>();
            paramMap.put("mobile", phone);
            paramMap.put("password", MD5Util.encrypt(password));
            paramMap.put("uuid", inviterUserUuid);

            String params = null;
            Map<String, String> retMap = EncryptDecrypt.encrypt(paramMap, encodeKey, signKey);
            if (retMap != null && "000".equals(retMap.get("resultCode"))) {
                params = retMap.get("resultData");
            } else {
                throw new BaseException(ResultConst.ENCRYPT_ERROR);
            }


            Map<String, Object> requestMap = new HashMap<>();
            requestMap.put("params", params);
            requestMap.put("registerOrigin", 11);


            Map<String, Object> resultMap = HttpUtil.requestByMap(regUrl, requestMap);

            if (resultMap == null || resultMap.size() == 0) {
                throw new BaseException(ResultConst.REG_ERROR);
            }

            if ((Boolean) resultMap.get("success") && "200".equals((String)resultMap.get("code"))) {
                String userUuid = (String) EncryptDecrypt.decrypt((String) resultMap.get("data"),encodeKey,signKey).get("resultData");

                user = new User();
                user.setNewRegUser(true);
                user.setPhone(phone);
                user.setActivityPoint(0);
                user.setActivityPointCoupon(0);
                user.setUserUuid(userUuid);
                user.setInviteUserUuid(inviterUserUuid);
                user.setActivityRegUser(true);
                user = userRepository.saveAndFlush(user);

                try {
                    messageService.sendRegSuccessMessage(phone, password);
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error("user regisitor success , but send default password message exception! phone:{},password:{},date:{}", phone, password, new Date());
                }
                return user;
            } else {
                throw new BaseException(Integer.parseInt(String.valueOf(resultMap.get("code"))), (String) resultMap.get("message"));
            }
        } else {
            throw new BaseException(ResultConst.USER_ALREADY_EXISTS);
        }

    }


    public static void main(String[] args) throws UnsupportedEncodingException {
        Map<String, String> map = new HashMap<>();
        map.put("uuid", "9744ff1e-e627-4b75-9aba-63af2c981b7b");
//        String data = "jZjhFarrCTHQBC3TT4uvvhmhTPHpqs8uv5szQQkvf1vuWoe7oUi2cfci7%2B2E4QnhyB5aihPjfQMWbs2iOZL8DLBBX7p9BYcB";
        String data = "jZ4fJgcM3z6wX%2FiwLwqmR0F7QYTeNhxkr8VuKdGLoYkqOHQiiUgmRIVY9KBYtPjD4FOGPQ%2BABuUtZPOACbfqqufbENQ448CJde9OhTMAihPYpStIsjS3COuM%2B2t9PTR1";
//        String data = "jZjhFarrCTGHTNJTU4KrR0SwVYInasm7og4rJqUrRqQgWc46mBIsG%2F3iwT%2BNCn0YrNExb6GWIwyRInziwk4lBa4b6balyupUA3AyNe6M6ib%2FeAZvU%2F7DeK484r273Brj";

//        data= URLDecoder.decode(data, "utf-8");
//        String retString = EncodeDecode.decodeValue("npullpwd", data);
//        System.out.println(retString);
//        String resultData = "jZjhFarrCTGHTNJTU4KrR0SwVYInasm7og4rJqUrRqQgWc46mBIsG%2F3iwT%2BNCn0YrNExb6GWIwyRInziwk4lBa4b6balyupUA3AyNe6M6ib%2FeAZvU%2F7DeK484r273Brj";
//        data = URLDecoder.decode(data, "utf-8");
//        String retString = decodeValue("npullpwd", data);
//        System.out.println(JSONObject.toJSONString(retString));
//        System.out.println(EncryptDecrypt.encrypt(map,"npullpwd","newpullappkey0000000000000000000"));
//        System.out.println(decodeValue("npullpwd", data));
        System.out.println(EncryptDecrypt.decrypt(data,"miapppwd","miniappkey0000000000000000000000"));
    }

}
