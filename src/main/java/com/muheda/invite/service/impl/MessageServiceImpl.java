package com.muheda.invite.service.impl;

import com.muheda.invite.common.exception.BaseException;
import com.muheda.invite.common.result.ResultConst;
import com.muheda.invite.common.util.PhoneFormatCheckUtils;
import com.muheda.invite.common.verificationCode.VerificationCodeSendUtil;
import com.muheda.invite.core.redis.RedisUtil;
import com.muheda.invite.service.MessageService;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


/**
 * Created by libin on 2018/7/14.
 */
@Service
public class MessageServiceImpl implements MessageService {


    @Autowired
    private RedisUtil redisUtil;

    @Value("${verificationCode.redis-key}")
    private String inviteVerificationCodePrefix;

    @Override
    public boolean sendVerificationCodeMessage(String phone,int type) throws Exception {
        boolean chinaPhoneLegal = PhoneFormatCheckUtils.isChinaPhoneLegal(phone);
        if (!chinaPhoneLegal) {
            throw new BaseException(ResultConst.PHONE_FORMAT_ERROR);
        }

        int random = RandomUtils.nextInt(1000, 9999);

        redisUtil.set(inviteVerificationCodePrefix + phone, random,180);

        String message = "您好，您的验证码是：" + random + "(3分钟内有效)";
        if (type == 1) {
            return VerificationCodeSendUtil.sendSmsMessage(phone, message);
        } else {
            return VerificationCodeSendUtil.sendVoiceMessage(phone, message);
        }
    }
    @Override
    public boolean sendRegSuccessMessage(String phone, String password) throws Exception {
        boolean chinaPhoneLegal = PhoneFormatCheckUtils.isChinaPhoneLegal(phone);
        if (!chinaPhoneLegal) {
            throw new BaseException(ResultConst.PHONE_FORMAT_ERROR);
        }

        String message = "您已成功加入睦合达，默认密码为"+password+"，为了确保您的账户安全可登陆点通宝APP修改密码";
        return VerificationCodeSendUtil.sendSmsMessage(phone, message);
    }

}
