package com.muheda.invite.service.impl;

import com.muheda.invite.common.exception.BaseException;
import com.muheda.invite.common.result.ResultConst;
import com.muheda.invite.entity.ExchangeInfo;
import com.muheda.invite.entity.Group;
import com.muheda.invite.entity.User;
import com.muheda.invite.repository.ExchangeRepository;
import com.muheda.invite.repository.GroupRepository;
import com.muheda.invite.repository.UserRepository;
import com.muheda.invite.service.ExchangeService;
import com.muheda.invite.service.GroupService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by libin on 2018/7/14.
 */
@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ExchangeService exchangeService;


    @Autowired
    private ExchangeRepository exchangeRepository;




    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Group addGroup(String userUuid) throws Exception {
        User user = userRepository.findByUserUuid(userUuid);
        if (user == null) {
            throw new BaseException(ResultConst.USER_NOT_EXISTS);
        }

        // 判断是否有未领取红包 如果有 要先领取
        int notExchangeCount = exchangeRepository.countByExchangeStatusAndUserUuid(0, userUuid);
        if (notExchangeCount >0) {
            throw new BaseException(ResultConst.HAVA_NOT_EXCHANGE_INFO);
        }

        //检查是否有进行中的团
        int notSuccessGroupCount = groupRepository.countByCreateUserUuidAndGroupStatus(userUuid, 1);
        if (notSuccessGroupCount > 0) {
            throw new BaseException(ResultConst.HAVA_NOT_SUCCESS_GROUP);
        }

        //判断今天创建了几个团
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date start = calendar.getTime();

        int count = groupRepository.countByCreateUserUuidAndCreateDateAfter(userUuid, start);
        if (count >= 5) {
            throw new BaseException(ResultConst.CREATE_GROUP_SUM_CAN_NOT_MORE_THAN_FIVE);
        }




        Group group = new Group();
        group.setCreateUserUuid(userUuid);
        group.setGroupUuid(UUID.randomUUID().toString());
        group.setCreateUserPhone(user.getPhone());
        group = groupRepository.saveAndFlush(group);

        if (user.isNewRegUser()) {
            user.setNewRegUser(false);
            userRepository.save(user);
        }

        return group;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Group joinGroup(String userUuid, String groupUuid) throws Exception {
        User user = userRepository.findByUserUuid(userUuid);
        if (user == null) {
            throw new BaseException(ResultConst.USER_NOT_EXISTS);
        }

        if (!user.isNewRegUser()) {
            throw new BaseException(ResultConst.USER_IS_NOT_NEW_REG_USER);
        }

        Group group = groupRepository.findByGroupUuid(groupUuid);
        if (group == null) {
            throw new BaseException(ResultConst.GROUP_NOT_EXISTS);
        }

        if (!StringUtils.isAnyEmpty(group.getInviteUser1Uuid(),group.getInviteUser2Uuid())) {
            throw new BaseException(ResultConst.GROUP_ALREADY_FULL);
        }



        if (StringUtils.isEmpty(group.getInviteUser1Uuid())) {
            group.setInviteUser1Uuid(userUuid);
            group.setInviteUser1JoinDate(new Date());
            group.setInviteUser1Phone(user.getPhone());
            group = groupRepository.saveAndFlush(group);
            user.setNewRegUser(false);
            userRepository.save(user);
            return group;
        }

        if (StringUtils.isEmpty(group.getInviteUser2Uuid()) ) {
            group.setInviteUser2Uuid(userUuid);
            group.setInviteUser2JoinDate(new Date());
            group.setInviteUser2Phone(user.getPhone());
            group.setGroupSuccessDate(new Date());
            group.setGroupStatus(2);


            group =  groupRepository.saveAndFlush(group);
            user.setNewRegUser(false);
            userRepository.save(user);
            // 瓜分奖励
            List<String> groupMemberUuids = new ArrayList<>();
            groupMemberUuids.add(group.getCreateUserUuid());
            groupMemberUuids.add(group.getInviteUser1Uuid());
            groupMemberUuids.add(group.getInviteUser2Uuid());
            exchangeService.groupSuccess(group.getId(),groupUuid, group.getCreateUserUuid(), groupMemberUuids);

            return group;
        }
        return group;
    }

    @Override
    public Group gerUserBelongGroup(String userUuid) throws Exception {

        List<Group> groups = groupRepository.findByUserUuidAndGroupStatus(userUuid);

        for (Group group : groups) {
            if (group==null) continue;
            if ( group.getGroupStatus() == 2) {
                List<ExchangeInfo> exchangeInfos = exchangeRepository.findByGroupUuidAndUserUuidAndExchangeStatus(group.getGroupUuid(), userUuid,0);
                if (exchangeInfos != null  && exchangeInfos.size()>0) {
                    group.setUserExchangeStatus(0);
                    group.setUserNotExchangeInfos(exchangeInfos);
                } else {
                    group.setUserExchangeStatus(1);
                }
            }
        }

        for (Group group : groups) {
            if (group==null) continue;
            if (group.getUserExchangeStatus() == 0) {
                return group;
            }
        }

        return null;
    }
}
