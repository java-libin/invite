package com.muheda.invite.service;

import java.io.IOException;

/**
 * Created by libin on 2018/7/14.
 */
public interface MessageService {
    boolean sendVerificationCodeMessage(String phone,int type) throws Exception;

    boolean sendRegSuccessMessage(String phone, String password) throws Exception;
}
