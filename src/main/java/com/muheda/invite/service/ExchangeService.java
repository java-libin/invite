package com.muheda.invite.service;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by libin on 2018/7/14.
 */
public interface ExchangeService {
    @Transactional(propagation = Propagation.REQUIRED)
    void groupSuccess(int groupId, String groupUuid, String organizerUserUuid, List<String> groupMemberUuids) throws Exception;
}
