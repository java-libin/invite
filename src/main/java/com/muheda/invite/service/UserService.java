package com.muheda.invite.service;

import com.muheda.invite.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by libin on 2018/7/11.
 */
public interface UserService {

    Page<User> findAll(Pageable page) throws Exception;


    User findByUserUuid(String userUuid) throws Exception;

    User userLogin(String phone, String password) throws Exception;

    User userReg(String phone, String smsCode, String inviterUserUuid)throws Exception;
}
