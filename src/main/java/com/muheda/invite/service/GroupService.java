package com.muheda.invite.service;

import com.muheda.invite.entity.Group;

/**
 * Created by libin on 2018/7/14.
 */
public interface GroupService  {
    Group addGroup(String userUuid) throws Exception;

    Group joinGroup(String userUuid, String groupUuid) throws Exception;

    Group gerUserBelongGroup(String userUuid)  throws Exception;
}
